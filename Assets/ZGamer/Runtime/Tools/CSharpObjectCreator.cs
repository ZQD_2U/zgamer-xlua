﻿using System;
using XLua;

// ReSharper disable once CheckNamespace
[LuaCallCSharp]
public class CSharpObjectCreator
{
    /// <summary>
    /// new DateTime
    /// </summary>
    /// <param name="year"></param>
    /// <param name="month"></param>
    /// <param name="day"></param>
    /// <param name="hour"></param>
    /// <param name="minute"></param>
    /// <param name="second"></param>
    /// <param name="kind">0: UTC 1: Local</param>
    /// <returns></returns>
    public static DateTime CreateDateTime(
        int year,
        int month,
        int day,
        int hour,
        int minute,
        int second,
        int kind = 0)
    {
        DateTimeKind curKind = kind == 0 ? DateTimeKind.Utc : DateTimeKind.Local;
        return new DateTime(year, month, day, hour, minute, second, curKind);
    }
}