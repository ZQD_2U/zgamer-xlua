
[XLua.LuaCallCSharp]
// ReSharper disable once CheckNamespace
public static class CsExtension
{
    public static bool Null(this UnityEngine.Object obj)
    {
        return obj == null;
    }
}