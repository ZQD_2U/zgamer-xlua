﻿using UnityEngine;
// ReSharper disable once CheckNamespace
[XLua.LuaCallCSharp]
public class Debugger
{
    public static void Log(string s, params object[] p)
    {
        Debug.LogFormat($"Frame {Time.frameCount} -- {s}", p);
    }

    public static void LogError(string s, params object[] p)
    {
        Debug.LogErrorFormat($"Frame {Time.frameCount} -- {s}", p);
    }

    public static void LogWarning(string s, params object[] p)
    {
        Debug.LogWarningFormat($"Frame {Time.frameCount} -- {s}", p);
    }
}