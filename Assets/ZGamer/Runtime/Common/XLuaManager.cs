﻿using System.IO;
using System.Text;
using FuXi;
using UnityEngine;
using XLua;

// ReSharper disable once CheckNamespace
public class XLuaManager : MonoBehaviour
{
    public static readonly XLuaManager Instance = SingletonCreator.CreateSingleton<XLuaManager>();
    private LuaEnv _env = null;
    private LuaUpdater _updater = null;

    private LuaTable scriptEnv;

    public void StartEnv()
    {
        InitEnv();
        InitLuaCSharpAccess();
        InitGameManager();
        StartGame();
    }

    /// <summary>
    /// Lua 重启
    /// </summary>
    public void RestartEnv()
    {
        Dispose();
        StartEnv();
    }

    //初始化Lua虚拟机
    private void InitEnv()
    {
        _env = new LuaEnv();
        _env.AddLoader(CustomLoader);
    }

    private void InitLuaCSharpAccess()
    {
        LuaAccessArrAPI.RegisterPinFunc(this._env.L);
    }

    /// <summary>
    /// init common model ...
    /// </summary>
    private void InitGameManager()
    {
        LoadScript(PathConfig.LuaCommonFileName);
        _updater = gameObject.GetComponent<LuaUpdater>();
        if (_updater == null)
        {
            _updater = gameObject.AddComponent<LuaUpdater>();
        }
        _updater.Init(_env);
    }
    
    private void StartGame()
    {
        LoadScript(PathConfig.LuaEntryFileName);
        SafeDoString("GameMain.Start()");
    }

    private void LoadScript(string fileName)
    {
        SafeDoString($"require '{fileName}'");
    }
    
    private void SafeDoString(string scriptContent)
    {
        if (_env == null) return;
        try
        {
            _env.DoString(scriptContent);
        }
        catch (System.Exception ex)
        {
            Debug.LogError($"xLua exception : {ex.Message}\n {ex.StackTrace}");
        }
    }

    private static byte[] CustomLoader(ref string fileName)
    {
        if (fileName.Equals("emmy_core"))
        {
            return null;
        }
        fileName = fileName.Replace('.', '/');
        return FxManager.RuntimeMode == RuntimeMode.Editor ? DevelopLoader(ref fileName) : RuntimeLoader(ref fileName);
    }

    private static byte[] RuntimeLoader(ref string fileName)
    {
        var file = string.Format(PathConfig.RunTimeLuaFilePath, fileName);
        var asset = FxAsset.LoadSync<TextAsset>(file);
        return (asset.asset as TextAsset)?.bytes;
    }
    
    private static byte[] DevelopLoader(ref string fileName)
    {
        var file = string.Format(PathConfig.EditorLuaFilePath, fileName); 
        return File.Exists(file) ? Encoding.UTF8.GetBytes(File.ReadAllText(file)) : null;
    }

    private void Update()
    {
        if (this._env == null) return;

        if (Time.frameCount % 10 == 0)
        {
            this._env.Tick();
        }
        if (Time.frameCount % 100 == 0)
        {
            this._env.FullGc();
        }
    }

    private void Dispose()
    {
        if (_updater!= null)
        {
            _updater.Dispose();
        }
        if (_env == null) return;
        try
        {
            _env.Dispose();
            _env = null;
        }
        catch (System.Exception ex)
        {
            Debug.LogError($"xLua exception : {ex.Message}\n {ex.StackTrace}");
        }
    }

    private void OnApplicationQuit()
    {
        SafeDoString("GameMain.OnApplicationQuit()");
    }
}