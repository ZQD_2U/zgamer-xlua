﻿using System;
using UnityEngine;
using XLua;

// ReSharper disable once CheckNamespace
public class LuaUpdater : MonoBehaviour
{
    private Action<float, float> luaUpdate = null;
    private Action luaLateUpdate = null;
    private Action<float> luaFixedUpdate = null;

    private Action luaEcsButton = null;

    public void Init(LuaEnv env)
    {
        luaUpdate      = env.Global.Get<Action<float, float>>("Update");
        luaLateUpdate  = env.Global.Get<Action>("LateUpdate");
        luaFixedUpdate = env.Global.Get<Action<float>>("FixedUpdate");
        luaEcsButton   = env.Global.Get<Action>("SystemEscBtn");
    }

    private void Update()
    {
        if (luaUpdate == null) return;
        try
        {
            luaUpdate(Time.deltaTime, Time.unscaledDeltaTime);
        }
        catch (Exception e)
        {
            Debug.LogError($"LuaUpdate error: {e.Message}\n{e.StackTrace}");
        }
        if (Input.GetKey(KeyCode.Escape)) luaEcsButton?.Invoke();
    }
    private void LateUpdate()
    {
        if (luaLateUpdate == null) return;
        try
        {
            luaLateUpdate();
        }
        catch (Exception e)
        {
            Debug.LogError($"LuaUpdate error: {e.Message}\n{e.StackTrace}");
        }
    }
    private void FixedUpdate()
    {
        if (luaFixedUpdate == null) return;
        try
        {
            luaFixedUpdate(Time.fixedDeltaTime);
        }
        catch (Exception e)
        {
            Debug.LogError($"LuaUpdate error: {e.Message}\n{e.StackTrace}");
        }
    }

    public void Dispose()
    {
        luaUpdate = null;
        luaLateUpdate = null;
        luaFixedUpdate = null;
        luaEcsButton = null;
    }

    private void OnDestroy()
    {
        Dispose();
    }
}