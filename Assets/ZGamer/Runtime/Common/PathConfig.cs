﻿// ReSharper disable once CheckNamespace
public static class PathConfig
{
    /* ------------------------------ Lua File Path ---------------------------------- */
    
    public static readonly string EditorLuaFilePath  = "Assets/LuaScripts/{0}.lua";  // 编辑器下Lua文件路径
    public static readonly string RunTimeLuaFilePath = "Assets/ZGamer/BundleResources/LuaScripts/{0}.bytes";  // 编辑器下Lua文件路径
    public static readonly string LuaCommonFileName  = "Helper.Main";                // 通用Lua文件
    public static readonly string LuaEntryFileName   = "GameMain";                   // Lua入口文件
    
}