﻿using UnityEngine;

// ReSharper disable once CheckNamespace
public static class SingletonCreator
{
    private static readonly string SingletonRoot = "Boot";
    private static GameObject boot;
    private static Transform Boot
    {
        get
        {
            if (boot == null)
            {
                boot = GameObject.Find(SingletonRoot);
            }
            if (boot == null)
            {
                boot = new GameObject(SingletonRoot);
                Object.DontDestroyOnLoad(boot);
            }
            return boot.transform;
        }
    }

    public static T CreateSingleton<T>() where T : Component
    {
        var xLuaMgr = Boot.GetComponentInChildren<T>();
        if (xLuaMgr != null) { return xLuaMgr; }
        var mgr = Object.Instantiate(boot, Boot);
        mgr.name = typeof(T).ToString();
        xLuaMgr = mgr.AddComponent<T>();
        return xLuaMgr;
    }
}