﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;
using XLua;
using XLua.LuaDLL;

public static class LuaEnvValues
{
    // only set false if using Lua5.3 and LUA_32BITS is enabled, see luaconf.h.in
    public const bool Is64Bit = true;

    // only set true if using Luajit and LJ_GC64 is enabled
    public const bool IsGC64 = false;

    public const int LUA_TNUMBER = 3;
    public const int LUA_TTABLE = 5;

    public const int LUA_TNUMFLT = (LUA_TNUMBER | (0 << 4));
    public const int LUA_TNUMINT = (LUA_TNUMBER | (1 << 4));
}

[StructLayout(LayoutKind.Explicit, Size = 12)]
public struct LuaTValue64
{
    [FieldOffset(0)]
    public UInt64 u64;
    
    [FieldOffset(0)]
    public double n;

    [FieldOffset(0)]
    public long i;

    [FieldOffset(8)]
    public int tt_;
}

[StructLayout(LayoutKind.Sequential)]
public struct LuaTableRawDef
{
    public IntPtr next;

    public uint bytes;

    public uint sizearray;

    public IntPtr array;

    public IntPtr node;

    public IntPtr lastfree;

    public IntPtr metatable;

    public IntPtr gclist;
}

[LuaCallCSharp]
public unsafe class LuaAccessArrAPI
{
    public static void RegisterPinFunc(System.IntPtr L)
    {
        var name = "lua_safe_pin_bind";
        Lua.lua_pushstdcallcfunction(L, PinFunction);
        if (0 != Lua.xlua_setglobal(L, name))
        {
            throw new LuaException("call xLua_setGlobal fail!");
        }
    }

    public static int PinFunction(System.IntPtr L)
    {
        ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
        IntPtr tablePtr = Lua.lua_topointer(L, 1);
        LuaArrAccess gen_to_be_invoked = (LuaArrAccess) translator.FastGetCSObj(L, 2);
        if (tablePtr != IntPtr.Zero && Lua.lua_istable(L, 1))
        {
            gen_to_be_invoked.OnPin(tablePtr);
        }
        return 0;
    }

    public static LuaArrAccess CreateLuaShareAccess()
    {
        return new LuaArrAccess64();
    }
    
}

public unsafe class LuaArrAccess32 : LuaArrAccess
{
    
}

[LuaCallCSharp]
public unsafe class LuaArrAccess64 : LuaArrAccess
{
    private LuaTableRawDef* tableRawPtr;
    public override string ToString()
    {
        IntPtr ptr = (IntPtr) tableRawPtr;
        return "LuaTablePin64" + ptr.ToString();
    }

    public override void OnPin(IntPtr tablePtr)
    {
        tableRawPtr = (LuaTableRawDef*) tablePtr;
    }

    public override void SetDouble(int index, double value)
    {
        if (tableRawPtr != null && index > 0 && index <= tableRawPtr -> sizearray)
        {
            index -= 1;
            LuaTValue64* v = (LuaTValue64*) tableRawPtr -> array + index;
            v->n = value;
            v->tt_ = LuaEnvValues.LUA_TNUMFLT;
        }
    }
}

[LuaCallCSharp]
public unsafe class LuaArrAccess
{
    public override string ToString()
    {
        return "LuaArrAccess Unknown type!";
    }
    
    public virtual void OnPin(IntPtr tablePtr){}
    
    public virtual void OnGC(){}
    
    public virtual void AutoDetectArch(){}

    public virtual bool IsValid()
    {
        return false;
    }

    public virtual uint GetArrayCapacity()
    {
        return 0;
    }

    public virtual int GetInt(int index)
    {
        return 0;
    }
    
    public virtual void SetInt(int index, int value){}

    public virtual double GetDouble(int index)
    {
        return 0.0;
    }
    
    public virtual void SetDouble(int index, double value){}
}