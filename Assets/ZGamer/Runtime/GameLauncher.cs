﻿using System.Collections;
using UnityEngine;
using XLua;

// ReSharper disable once CheckNamespace
public class GameLauncher : MonoBehaviour
{
    public FuXi.RuntimeMode RuntimeMode;
    IEnumerator Start()
    {
        //CS 单元测试入口
        CSUnitTest.CSUnitTestStart();
        // 游戏入口 启动 资源管理器
        yield return FuXi.FxManager.FxLauncherAsyncCo("FuXiAsset", "", RuntimeMode);
        // 启动 Lua 虚拟机
        XLuaManager.Instance.StartEnv();
    }
}

// 简单演示如何在C#访问lua数组
[LuaCallCSharp]
public class LuaTestBasicExample
{
    private LuaArrAccess Table;

    public void CSharpExample(LuaArrAccess arrAccess)
    {
        Debug.Assert(arrAccess != null);

        Table = arrAccess;

        arrAccess.SetDouble(123, 123.5);
        arrAccess.GetInt(1);
    }
}
