﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

public static class LayoutSizeTest
{
    public static unsafe void Start()
    {
        // Debug.Log($"bool:{sizeof(bool)}");
        // Debug.Log($"byte:{sizeof(byte)}");
        // Debug.Log($"char:{sizeof(char)}");
        // Debug.Log($"decimal:{sizeof(decimal)}");
        // Debug.Log($"double:{sizeof(double)}");
        // Debug.Log($"float:{sizeof(float)}");
        // Debug.Log($"int:{sizeof(int)}");
        // Debug.Log($"long:{sizeof(long)}");
        // Debug.Log($"sbyte:{sizeof(sbyte)}");
        // Debug.Log($"short:{sizeof(short)}");
        // Debug.Log($"uint:{sizeof(uint)}");
        // Debug.Log($"ulong:{sizeof(ulong)}");
        // Debug.Log($"ushort:{sizeof(ushort)}");

        // var st_seq = sizeof(Struct_Seq);
        // Debug.Log(st_seq);
        //
        // var st_exp = sizeof(Struct_Exp);
        // Debug.Log(st_exp);
        //
        // var st_auto = sizeof(Struct_Auto);
        // Debug.Log(st_auto);
        //
        // Struct_Exp exp = new Struct_Exp();
        // exp.i_1 = 1111;
        // exp.b_2 = false;
        // Debug.Log($"Exp:{exp.i_1}");
        
        //Debug.Log($"Intptr:{sizeof(IntPtr)}");

        // var binary = 0B11111111111111111111111111111111;
        // var octal = 0111;
        // var hex = 0XFFFF;
        // Debug.Log($"Binary:{binary} -- Octal:{octal} -- Hex:{hex}");
        
    }

    [StructLayout(LayoutKind.Sequential)]
    struct Struct_Seq
    {
        private bool b_1;
        private double i_1;
        private bool b_2;
    }
    
    [StructLayout(LayoutKind.Explicit)]
    private struct Struct_Exp
    {
        [FieldOffset(0)]
        public bool b_1;
        [FieldOffset(8)]
        public double i_1;
        [FieldOffset(8)]
        public bool b_2;
    }
    
    [StructLayout(LayoutKind.Auto)]
    private struct Struct_Auto
    {
        public bool b_1;
        public double i_1;
        public bool b_2;
    }
    
    //[StructLayout(LayoutKind.Sequential)]
    struct LayoutSizeDemo
    {
        //[FieldOffset(0)]
        private UInt32 u_32_1;
        private int i_1;
        private UInt32 u_32_2;
        private long l_1;
    }
}

