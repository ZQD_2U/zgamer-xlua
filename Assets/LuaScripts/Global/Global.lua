﻿---@public Global 全局加载模块

require "Core.Base.Class"
require "Core.Base.Singleton"
------------------------------------ Common -------------------------------------
require "Core.Entity"
require "Global.CSCallLua"

------------------------------------- Async -------------------------------------
require "Core.Async.Task"
require "Core.Async.Coroutine"
require "Core.Async.TimerManager"
require "Core.Async.UpdateCenter"

require "Core.Helper.IdGenerator"
require "Core.Helper.EntityHelper"
require "Core.Object.TimeInfo"
------------------------------------- Event -------------------------------------
require "Core.Event.InstMessenger"
require "Core.Event.Messenger"

----------------------------------- Resource ------------------------------------
require "Core.Resource.AssetManager"
require "Core.Resource.ObjectPool"

---------------------------------- Scene Core -----------------------------------
require "Core.Scene.SceneManager"
require "Core.Scene.Scenes"

------------------------------------ UI Core ------------------------------------
require "Core.UI.UIManager"
require "Core.UI.UIWinForm"
require "Core.UI.UILayers"
require "Core.UI.UILayer"

---------------------------------- UI Component ---------------------------------
require "Core.UIComponent.UICanvas"

------------------------------------- Game --------------------------------------
--- UI
require "Game.UI.UIConfig.UIWindows"
--- Data
require "Data.DataEventID"


require "Core.Game"
require "UnitTest.UnitLauncher"