﻿
return {
    Name        = "UIHomeForm",
    LayerName   = UILayers.BackGroundLayer.Name,
    PrefabPath  = "Assets/ZGamer/BundleResource/Prefabs/UI/UIHomeForm/UIHomeForm.prefab",
    Binder      = nil,
    Model       = nil,
    View        = require "Game.UI.UIHomeForm.UIHomeFormView",
    Ctrl        = nil,
}