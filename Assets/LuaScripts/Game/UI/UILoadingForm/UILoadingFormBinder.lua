﻿
local UILoadingFormBinder = Class("UILoadingFormBinder", Entity)

function UILoadingFormBinder:OnStart()
    self.slider = self.transform:Find("SliderBG/Handle"):GetComponent(typeof(CS.UnityEngine.UI.Image))
    self.desc   = self.transform:Find("Desc"):GetComponent(typeof(CS.UnityEngine.UI.Text))
end 

return UILoadingFormBinder