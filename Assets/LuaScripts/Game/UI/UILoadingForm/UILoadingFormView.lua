﻿
local UILoadingFormView = Class("UILoadingFormView", Entity)

function UILoadingFormView:OnStart(schedule)
    self.schedule = schedule
end

function UILoadingFormView:OnEnable()
    self.updateHandle = Game.UpdateCenter:AddUpdater(self.Update, self)
end

function UILoadingFormView:Update()
    self.binder.slider.fillAmount = self.schedule.progress
    self.binder.desc.text = self.schedule.desc
end

function UILoadingFormView:OnDisable()
    if self.updateHandle ~= nil then
        Game.UpdateCenter:RemoveUpdater(self.updateHandle)
    end
    self.updateHandle = nil
end

function UILoadingFormView:OnDestroy()
    self.base.OnDestroy(self)
end

return UILoadingFormView