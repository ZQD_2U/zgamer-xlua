﻿
return {
    Name        = "UILoadingForm",
    LayerName   = UILayers.TopLayer.Name,
    PrefabPath  = "Assets/ZGamer/BundleResource/Prefabs/UI/UILoadingForm/UILoadingForm.prefab",
    Binder      = require "Game.UI.UILoadingForm.UILoadingFormBinder",
    Model       = nil,
    View        = require "Game.UI.UILoadingForm.UILoadingFormView",
    Ctrl        = nil,
}