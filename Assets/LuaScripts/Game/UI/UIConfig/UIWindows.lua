﻿
---@class UIWindows readonly
UIWindows = {}

UIWindows.UILoadingForm     = require "Game.UI.UILoadingForm.UILoadingFormConfig"
UIWindows.UIHomeForm        = require "Game.UI.UIHomeForm.UIHomeFormConfig"

setmetatable(UIWindows, {__newindex = function() Debug.LogWarning("Don't insert new field into UIWindows! it's readonly.") end})