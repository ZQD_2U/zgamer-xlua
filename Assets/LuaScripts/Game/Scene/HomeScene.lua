﻿
local HomeScene = Class("HomeScene", Entity)

---@private void 异步执行
function HomeScene:OnStart(progressCb)
    coroutine.waitForFrames(10)
    progressCb(0.1)
    coroutine.waitForFrames(10)
    progressCb(0.2)
    coroutine.waitForFrames(10)
    progressCb(0.3)
    coroutine.waitForFrames(10)
    progressCb(0.4)
    coroutine.waitForFrames(10)
    progressCb(0.5)
    coroutine.waitForFrames(10)
    progressCb(0.6)
    coroutine.waitForFrames(10)
    progressCb(0.7)
    coroutine.waitForFrames(10)
    progressCb(0.8)
    coroutine.waitForFrames(10)
    progressCb(0.9)
    coroutine.waitForFrames(10)
    progressCb(1)
    UIManager.Instance:OpenWindow(UIWindows.UIHomeForm)
end 

function HomeScene:OnDestroy()
    
end

return HomeScene