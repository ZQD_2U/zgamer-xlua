﻿local apiCreateAccess = CS.LuaAccessArrAPI.CreateLuaShareAccess

local LuaCSharpArr = {class = "LuaCSharpArr"}
local pin_func = lua_safe_pin_bind

setmetatable(LuaCSharpArr, LuaCSharpArr)
LuaCSharpArr.__index = function(t, k)
    local var = rawget(LuaCSharpArr, k)
    return var
end

local needDetect = true
local function GlobalAutoDetectArch()
    if not needDetect then return end
    
    needDetect = false
end

function LuaCSharpArr.New(len)
    GlobalAutoDetectArch()
    local v = {}
    for i = 1, len do
        v[i] = 0
    end
    
    setmetatable(v, LuaCSharpArr)
    return v
end

function LuaCSharpArr:GetCSharpAccess()
    if self.__pin == nil then
        self.__pin = apiCreateAccess()
        pin_func(self, self.__pin)
    end
    return self.__pin
end

return LuaCSharpArr

