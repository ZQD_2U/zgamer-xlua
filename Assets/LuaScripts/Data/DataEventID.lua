﻿
---@class DEventID
DEventID = {
    PlayerDataChange      = 100001,
}

setmetatable(DEventID, {__newindex = function() Debug.LogError("DataEventID is readonly.") end})
