﻿---@public 单元测试启动器
---@private
UnitLauncher = {}

local ClassTest = require "UnitTest.ClassTest"
local EntityTest = require "UnitTest.EntityTest"
local StructTest = require "UnitTest.StructTest"
local CoroutineTest = require "UnitTest.CoroutineTest"
local LuaCSharpArrTest = require "UnitTest.LuaCSharpArrTest"

function UnitLauncher:StartUp()
    Debug.Log("------------------------启动单元测试--------------------------")
    
    --ClassTest:Run()
    --EntityTest:Run()
    --StructTest:Run()
    --CoroutineTest:Run()
    LuaCSharpArrTest:Run()
    
    Debug.Log("------------------------结束单元测试--------------------------")
end
