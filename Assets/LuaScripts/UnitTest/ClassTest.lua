﻿--- 单元测试 - Class 面向对象测试
local ClassTest = {}

---@class Person
local Person = Class("Person", Entity)

function Person:ctor()
    self.type = "instance"
    self.name = "-----"
end

---@param name string
function Person:SetName(name)
    self.a = name
end

function ClassTest:Run()
    local hero1 = Person.new("Hero1")
    local hero2 = Person.new("Hero2")
    
    local c = Game.IdGenerator
    
    
    Debug.Log(hero1.instanceId)
    Debug.Log(hero2.instanceId)
    
end
return ClassTest