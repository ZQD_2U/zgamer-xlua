﻿local LuaCSharpArr = require "Game.LuaCSharpAccess.LuaCSharpArr"

local LuaCSharpArrTest = {}

function LuaCSharpArrTest:Run()
    local testCase = CS.LuaTestBasicExample()
    
    local testArr = LuaCSharpArr.New(123)
    testArr[1] = 22.5
    testArr[123] = 9995
    
    local csharpAccess = testArr:GetCSharpAccess()
    testCase:CSharpExample(csharpAccess)
    Debug.Log("123: " .. testArr[123])
end

return LuaCSharpArrTest