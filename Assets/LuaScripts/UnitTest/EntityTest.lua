﻿--- 实体组件测试

local EntityTest = {}

---@class Player
local Player = Class("Player", Entity)

function Player:ctor()
    self.name = "player"
end

function Player:SetName(name)
    
end

function EntityTest:Run()
    local sl = EntityHelper.Instance:CreateEntity(Player, CS.UnityEngine.GameObject())
    sl.name = "xxxxx"
    Debug.Log("Player Name: " .. sl.name, "lalla")
    Debug.Log("Player Id: " .. sl.id)
    
    local callTb = {}
    callTb.__call = function(self, a)
        return a + 1
    end
    local mt = setmetatable({}, callTb)
    Debug.Log("Call:" .. mt(1))
end

return EntityTest