﻿--- 基础数据结构测试

local StructTest = {}

function StructTest:Run()
    Debug.Log("------------------------Struct test------------------")
    self:TestList()
    --self:TestLinkedList()
    --self:TestStack()
    --self:TestDictionary()
end

---@private 测试 List
function StructTest:TestList()
    local list = List:new()
    list:Add("cbd")
    list:Add("王中王")
    list:Add("王老吉")
    list:Add("qbp")
    list:Add("和旗胜")
    list:Add("tre")
    list:Add("tae")
    list:Add("mhg")
    list:RemoveAt(list.Length)
    --Debug.StartWatch()
    --for i = 1, 1000 do
    --    list:Add(math.random(0, 100000))
    --end
    --Debug.StopWatch("插入10000元素耗时")
    --list:RemoveAll(2)
    --for i, v in ipList(list) do
    --    Debug.Log(i.."|"..v)
    --end
    --Debug.StartWatch()
    --list:OrderBy(function(v) return v end)
    --Debug.StopWatch("冒泡排序耗时")
    --Debug.Log("sorting.............")

    --Debug.StartWatch()
    --table.sort(list.L, function(a, b) return a < b end)
    --Debug.StopWatch("Lua 自带排序")

    --Debug.StartWatch()
    --list:OrderBy(function(v) return v end, true)
    --Debug.StopWatch("快速排序耗时")
    --
    --local function a(x)
    --    x()
    --end
    --local ts = {a = 0}
    --Debug.StartWatch()
    --for i = 1, 1000000 do
    --    a(function() ts.a = ts.a + i end)
    --end
    --Debug.StopWatch("调用1000000次func耗时")
    --
    --Debug.StartWatch()
    --for i, v in ipList(list) do
    --    Debug.Log(i.."|"..v)
    --end
    --Debug.StopWatch("打印日志耗时")
    
    --Debug.Log("Byte:" .. string.byte("abcd", 1,4))
    --Debug.Log("Byte:" .. string.byte("abcd", 2,4))
    --Debug.Log("Byte:" .. string.byte("abcd", 3,4))
    
    --Debug.Log(list:ValueOfIndex(2))
    --Debug.Log(list:IndexOfValue(12))
    --Debug.Log(list:LastIndexOfValue(2))
end

---@private 测试双向链表LinkedList
function StructTest:TestLinkedList()
    local list = LinkedList:new(LuaType.Number)
    list:Add(1)
    list:Add(3)
    list:Add(2)
    list:Add(5)
    list:Add(8)
    list:Add(2)
    list:Add(12)
    list:Add(2)
    --list:RemoveAll(2)
    
    Debug.StartWatch()
    list:OrderBy(function(v) return v end)
    Debug.StopWatch("链表排序耗时")
    
    Debug.StartWatch()
    for _, v in ipLkList(list) do
        if v == 2 then
            list:Remove(v)
        end
        Debug.Log(v)
    end
    Debug.StopWatch("输出日志耗时")
    Debug.StartWatch()
    for _, v in ipLkList(list) do
        Debug.Log(v)
    end
    Debug.StopWatch("输出日志耗时")
end

---@private 测试栈Stack
function StructTest:TestStack()
    local list = Stack:new(LuaType.Number)
    list:Push(1)
    list:Push(2)
    list:Push(2)
    list:Push(2)
    list:Push(8)
    list:Push(2)
    list:Push(12)
    list:Push(2)
    local p = list:Pop()
    Debug.Log("Pop p = " .. p)
    for d, v in ipStack(list) do
        Debug.Log("Depth:" .. d .. " | " .. v)
    end
end

---@private 测试字典Dictionary
function StructTest:TestDictionary()
    local dic = Dictionary:new(LuaType.Number, LuaType.String)
    dic:Add(1, "ccc")
    dic:Add(5, "bbb")
    dic:Add(3, "vvv")
    dic:Add(78, "aaa")
    dic:Add(34, "aaa")
    dic:Add(22, "ddd")
    dic:Add(523, "eee")
    dic:Add(2, "ttt")
    dic:Remove(3)
    dic:RemoveValue("aaa")
    Debug.Log("Count = " .. dic:Count())
    Debug.Log("Contains k = " .. tostring(dic:ContainsKey(523)))
    Debug.Log("Contains v = " .. tostring(dic:ContainsValue("aaa")))
    for k, v in ipDictionary(dic) do
        Debug.Log("K " .. k .. " | " .. v)
    end
end

return StructTest