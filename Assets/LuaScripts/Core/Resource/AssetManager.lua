﻿---资源管理器
---@class AssetManager
AssetManager = Class("AssetManager", Singleton)
local HAsset = CS.FuXi.FxAsset
local HScene = CS.FuXi.FxScene

---@private
function AssetManager:ctor()
    ---资源缓存
    self._cache_asset = Dictionary:new(LuaType.String, nil)
end

---@public UserData 同步加载资源
---@param path string 资源加载路径
---@generic Object
---@param type Object 资源类型
---@return Object
function AssetManager:LoadAsset(path, type)
    local hotAsset = self._cache_asset:GetValue(path)
    if Null(hotAsset) then
        hotAsset = HAsset.Load(path, type)
        self._cache_asset:Add(path, hotAsset)
    end
    return hotAsset.asset
end

---@param path string 资源加载路径
---@generic Object
---@param type Object 资源类型
---@generic Object
---@param callBack fun(obj:Object)
function AssetManager:LoadAssetAsync(path, type, callBack)
    local hotAsset = self._cache_asset:GetValue(path)
    if Null(hotAsset) then
        hotAsset = HAsset.LoadAsync(path, type, function(ha)
            callBack(ha.asset)
        end)
        self._cache_asset:Add(path, hotAsset)
    else
        callBack(hotAsset.asset)
    end
end

---@public HAsset 异步加载资源，协程内部使用
---@param path string 资源加载路径
---@generic Object
---@param type Object 资源类型
---@return Object
function AssetManager:AwaitLoadAsset(path, type)
    local hotAsset = self._cache_asset:GetValue(path)
    if Null(hotAsset) then
        hotAsset = HAsset.LoadCo(path, type)
        coroutine.waitAsyncOperation(hotAsset)
        self._cache_asset:Add(path, hotAsset)
    end
    return hotAsset.asset
end

---@param path string
---@param progressCb fun(co:thread, progress:number)
---@param additive boolean
function AssetManager:AwaitLoadScene(path, progressCb, additive)
    local hScene = HScene.LoadSceneCo(path, additive or false)
    coroutine.waitAsyncOperation(hScene, progressCb)
end

function AssetManager:LoadSprite(path)
    return self:LoadAsset(path, typeof(CS.UnityEngine.Sprite))
end

---@public
---@param path string 图片路径
---@generic Sprite
---@param callBack fun(obj:Sprite)
function AssetManager:LoadSpriteAsync(path, callBack)
    self:LoadAssetAsync(path, typeof(CS.UnityEngine.Sprite), callBack)
end

---@public userdata 运行在 协程内，等待加载完成 并返回结果
---@param path string
function AssetManager:AwaitLoadSprite(path)
    return self:AwaitLoadAsset(path, typeof(CS.UnityEngine.Sprite))
end

function AssetManager:LoadPrefab(path)
    return self:LoadAsset(path, typeof(CS.UnityEngine.GameObject))
end

---@public
---@param path string 预制体路径
---@generic Object
---@param callBack fun(obj:Object)
function AssetManager:LoadPrefabAsync(path, callBack)
    self:LoadAssetAsync(path, typeof(CS.UnityEngine.GameObject), callBack)
end

---@public userdata 运行在 协程内，等待加载完成 并返回结果
---@param path string
function AssetManager:AwaitLoadPrefab(path)
    return self:AwaitLoadAsset(path, typeof(CS.UnityEngine.GameObject))
end

---@param path string 预制体路径
---@generic transform
---@param root transform 父节点transform
function AssetManager:Instantiate(path, root)
    assert(type(path) == LuaType.String and #path > 0, "asset path is error.")
    local obj = self:LoadPrefab(path)
    return CS.UnityEngine.Object.Instantiate(obj, root)
end

---@param path string 预制体路径
---@generic transform
---@param root transform 父节点transform
---@generic Object
---@param callback fun(obj:Object)
function AssetManager:InstantiateAsync(path, root, callback)
    assert(type(path) == LuaType.String and #path > 0, "asset path is error.")
    assert(type(callback) == LuaType.Function, "callback is nil.")
    self:LoadPrefabAsync(path, function(asset)
        local inst = CS.UnityEngine.Object.Instantiate(asset, root)
        callback(inst)
    end)
end

---@public userdata 运行在 协程内，等待加载完成 并返回实例化对象
---@param path string 预制体路径
---@generic transform
---@param root transform 父节点transform
---@generic Object
---@return Object
function AssetManager:AwaitInstantiate(path, root)
    assert(type(path) == LuaType.String and #path > 0, "asset path is error.")
    local obj = self:AwaitLoadPrefab(path)
    return CS.UnityEngine.Object.Instantiate(obj, root)
end

---@public void 释放资源
---@param path string
function AssetManager:ReleaseWithPath(path)
    local hotAsset = self._cache_asset:GetValue(path)
    if Null(hotAsset) then return end
    hotAsset:Release()
    self._cache_asset:Remove(path)
end

---@public void 清理缓存
function AssetManager:Clear()
    for _, v in ipDictionary(self._cache_asset) do
        v:Release()
    end
    self._cache_asset:Clear()
end

---@type AssetManager
AssetManager.Instance = AssetManager:GetInstance()