﻿
---@class ObjectPool 对象池
ObjectPool = Class("ObjectPool", Singleton)

---@private
function ObjectPool:ctor()
    self._root_obj = nil
    self._cache_inst = Dictionary:new(LuaType.String)
    ---弱引用, 源对象 销毁后 防止 当前表 还持有对象引用, 对象 路径 映射表
    self._inst2_path = setmetatable({}, {__mode = "k"})
end

---@private
function ObjectPool:GetCacheRootTransform()
    if not Null(self._root_obj) then
        return self._root_obj.transform
    end
    self._root_obj = NewGameObject("_ObjectPoolCacheRoot_")
    return self._root_obj.transform
end

---@private
function ObjectPool:GetFromCache(path, root)
    ---@type List
    local cache_list = self._cache_inst:GetValue(path)
    local inst = nil
    if cache_list == nil then return inst end
    
    if cache_list.Length > 0 then
        inst = cache_list:RemoveAt(cache_list.Length)
    end
    if Null(inst) then return nil end
    if not Null(root) then
        inst.transform:SetParent(root)
    else
        inst.transform:SetParent(self:GetCacheRootTransform())
    end
    inst:SetActive(true)
    return inst
end

---@private
function ObjectPool:InstantiateInternal(path, obj, root)
    if Null(obj) then
        Debug.LogError("object is null.")
        return nil
    end
    local inst = nil
    if not Null(root) then
        inst = CS.UnityEngine.Object.Instantiate(obj, root)
    else
        inst = CS.UnityEngine.Object.Instantiate(obj, self:GetCacheRootTransform())
    end
    self._inst2_path[obj] = path
    return inst
end

---@param path string 预制体路径
---@generic transform
---@param root transform 父节点transform
function ObjectPool:Instantiate(path, root)
    assert(type(path) == LuaType.String and #path > 0, "asset path is error.")
    local obj = self:GetFromCache(path, root)
    if not Null(obj) then return obj end
    obj = AssetManager.Instance:LoadPrefab(path)
    return self:InstantiateInternal(path, obj, root)
end

---@param path string 预制体路径
---@generic transform
---@param root transform 父节点transform
---@generic Object
---@param callback fun(obj:Object)
function ObjectPool:InstantiateAsync(path, root, callback)
    assert(type(path) == LuaType.String and #path > 0, "asset path is error.")
    assert(type(callback) == LuaType.Function, "callback is nil.")
    local obj = self:GetFromCache(path, root)
    if not Null(obj) then callback(obj) end
    AssetManager.Instance:LoadPrefabAsync(path, function(asset)
        obj = self:InstantiateInternal(path, asset, root)
        callback(obj)
    end)
end

---@public userdata 运行在 协程内，等待加载完成 并返回实例化对象
---@param path string 预制体路径
---@generic transform
---@param root transform 父节点transform
---@generic Object
---@return Object
function ObjectPool:AwaitInstantiate(path, root)
    assert(type(path) == LuaType.String and #path > 0, "asset path is error.")
    local obj = self:GetFromCache(path, root)
    if not Null(obj) then return obj end
    obj = AssetManager.Instance:AwaitLoadPrefab(path)
    return self:InstantiateInternal(path, obj, root)
end

---@public void 回收对象到对象池
---@generic Object
---@param obj Object 待回收对象
function ObjectPool:RecycleObject(obj)
    if Null(obj) then return end
    local path = self._inst2_path[obj]
    obj:SetActive(false)
    obj.transform:SetParent(self:GetCacheRootTransform())
    if path == nil then return end
    local cache_list = self._cache_inst:GetValue(path)
    if cache_list == nil then
        cache_list = List:new()
        self._cache_inst:Add(path, cache_list)
    end
    cache_list:Add(obj)
end

---@public void 清理销毁 对象池
function ObjectPool:DestroyAll()
    self._inst2_path = setmetatable({}, {__mode = "k"})
    for _, caList in ipDictionary(self._cache_inst) do
        for _, v in ipList(caList) do
            CS.UnityEngine.GameObject.Destroy(v)
        end
        caList:Clear()
    end
    self._cache_inst:Clear()
end

---@type ObjectPool
ObjectPool.Instance = ObjectPool:GetInstance()