﻿
---@class Entity 实体基类
Entity = Class("Entity")

---@class EntityType
EntityType = {
    ---@public Entity 实体
    Entity      = "Entity",
    ---@public Component 组件
    Component   = "Component",
}
---@private 构造函数
function Entity:ctor(...)
    self.parent     = nil
    ---@field gameObject userdata 实体持有的C#对象
    self.gameObject = nil
    self.transform  = nil
    --- 同一个组件 同一个实体只能包含一个
    self.components = Dictionary:new()
    self.entityList = List:new()
    self.childList  = List:new()
    self.id         = 0
    self.instanceId = 0
    self.type       = EntityType.Entity
end

---@public 实体Start
function Entity:OnStart(...) end

function Entity:OnEnable()
    if self.gameObject == nil then return end
    self.gameObject:SetActive(true)
end

function Entity:Update() end

function Entity:OnDisable()
    if self.gameObject == nil then return end
    self.gameObject:SetActive(false)
end

---@public 添加组件
---@generic Component
---@param cmp Entity|Component inherit from entity or CSharp component
function Entity:AddComponent(cmp, ...)
    local comp = self.components:GetValue(cmp)
    if comp ~= nil then 
        Debug.LogWarning("component is exist, don't add repeat!")
        return
    end
    if type(cmp) == LuaType.UserData then
        comp = self.gameObject:AddComponent(cmp)
    elseif cmp == Entity or cmp.base == Entity then
        comp = EntityHelper.Instance:CreateComponent(cmp, self.gameObject, ...)
        comp:OnStart(...)
    else
        Debug.LogError("component is not exist!")
        return
    end
    self.components:Add(cmp, comp)
    return comp
end

---@public 添加组件
---@param cmp Entity inherit from entity
function Entity:GetComponent(cmp)
    local comp = self.components:GetValue(cmp)
    if Null(comp) then
        Debug.LogError("Component is not exist -->" .. tostring(cmp))
    end
    return comp
end

---@public 移除组件
---@param type Entity 要移除的 组件类型
function Entity:RemoveComponent(type)
    self.components:Remove(type)
end

---@public entity 添加实体
---@param entityType table|Entity 实体类型
---@return Entity
function Entity:AddNonEntity(entityType)
    if entityType.__cType ~= ClassType.class then
        Debug.LogWarning("type is instance.")
        return nil
    end
    local entity = EntityHelper.Instance:CreateEntity(entityType)
    entity:OnStart()
    if self.entityList:Contains(entity) then
        Debug.LogWarning("entity is exist.")
        return entity
    end
    self.entityList:Add(entity)
    return entity
end

---@public void 添加实体
---@generic instance
---@param entity instance 通过 EntityHelper 生成的 实体
function Entity:AddEntity(entity)
    if entity.__cType ~= ClassType.instance then
        Debug.LogWarning("type is not instance.")
        return
    end
    if self.entityList:Contains(entity) then
        Debug.LogWarning("entity is exist.")
        return
    end
    self.entityList:Add(entity)
end

function Entity:RemoveEntity(entity)
    if not self.entityList:Contains(entity) then return end
    self.entityList:Remove(entity)
end

---@param entity Entity
function Entity:AddChild(entity)
    if type.__cType ~= ClassType.instance then
        Debug.LogWarning("type is not instance.")
        return
    end
    if not Null(entity.gameObject) and not Null(self.gameObject) then
        entity.transform:SetParent(self.transform)
        entity.parent = self.gameObject
    end
    self.childList:Add(entity)
end

function Entity:RemoveChild(entity)
    if not self.childList:Contains(entity) then return end
    entity.parent = nil
    self.childList:Remove(entity)
end

---@private
function Entity:OnDestroy()
    self:Delete()
    for _, v in ipDictionary(self.components) do
        v:OnDestroy()
    end
    self.components:Clear()
    for _, v in ipList(self.entityList) do
        v:OnDestroy()
    end
    self.entityList:Clear()
    for _, v in ipList(self.childList) do
        v:OnDestroy()
    end
    self.childList:Clear()
    if not Null(self.gameObject) then
        CS.UnityEngine.GameObject.DestroyImmediate(self.gameObject);
    end
    self.gameObject = nil
    self.transform = nil
    self.parent = nil
end

---@private
function Entity:Delete()

end