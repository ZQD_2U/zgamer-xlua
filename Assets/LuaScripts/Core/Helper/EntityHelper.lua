﻿
---@class EntityHelper
EntityHelper = Class("EntityHelper", Singleton)

---@private
---@param et Entity 实体
function EntityHelper:CreateEntityInternal(et, obj, ...)
    assert(type(et) == LuaType.Table, "entity is not a table or class.")
    assert(et == Entity or et.base == Entity, "cannot create entity of nonEntity, classname: " .. et.__cname)
    local entity = et.new(...)
    entity.id    = IdGenerator.Instance:GenerateId()
    if not Null(obj) then
        entity.instanceId = obj:GetInstanceID()
        entity.gameObject = obj
        entity.transform  = obj.transform
    else
        entity.instanceId = 0
    end
    return entity
end

---@public 创建组件
---@param type Entity
---@param obj userdata gameObject
---@return Entity|any
function EntityHelper:CreateComponent(type, obj, ...)
    local component = EntityHelper:CreateEntityInternal(type, obj, ...)
    component.type  = EntityType.Component
    return component
end

---@public 创建实体
---@param type Entity 实体类型
---@param obj userdata gameObject
---@return Entity|any
function EntityHelper:CreateEntity(type, obj, ...)
    local entity = EntityHelper:CreateEntityInternal(type, obj, ...)
    entity.type  = EntityType.Entity
    return entity
end

---@public 创建实体
---@param type Entity 实体类型
---@return Entity
function EntityHelper:CreateEntityWithParent(type, parent, ...)
    
end

---@type EntityHelper
EntityHelper.Instance = EntityHelper:GetInstance()