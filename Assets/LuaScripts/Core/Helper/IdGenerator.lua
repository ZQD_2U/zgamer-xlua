﻿
---@class IdGenerator
IdGenerator = Class("IdGenerator", Singleton)

function IdGenerator:ctor()
    local epoch1970       = CS.CSharpObjectCreator.CreateDateTime(1970, 1, 1, 0, 0, 0, 0).Ticks / 10000
    self.epoch2020        = CS.CSharpObjectCreator.CreateDateTime(2020, 1, 1, 0, 0, 0, 0).Ticks / 10000 - epoch1970
    self.instanceIdEpoch  = CS.CSharpObjectCreator.CreateDateTime(CS.System.DateTime.Now.Year, 1, 1, 0, 0, 0, 0).Ticks / 10000 - epoch1970
    
    self.lastIdTime = 0
    self.idThisSecCount = 0
    self.value = 0
end

function IdGenerator:TimeSince2020()
    local time = (TimeInfo.Instance:ClientNow() - self.epoch2020) / 1000
    return Mathf.Round(time)
end

function IdGenerator:GenerateId()
    local time = self:TimeSince2020()
    if time == self.lastIdTime then
        self.idThisSecCount = self.idThisSecCount + 1
    else
        self.lastIdTime = time
        self.idThisSecCount = 1
    end
    if self.idThisSecCount == 65535 then
        Debug.LogError("Id count per sec overflow: " .. self.idThisSecCount)
    end
    self.value = self.value + 1
    if self.value > 65535 then
        self.value = 0
    end
    
    local result = 0
    local process = 0
    result = result | self.value
    result = result | process << 16
    result = result | time << 34
    return result
end

---@type IdGenerator
IdGenerator.Instance = IdGenerator:GetInstance()