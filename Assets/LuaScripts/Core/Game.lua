﻿---游戏全局管理器
---@class Game
Game = {}

Game.TimeInfo           = TimeInfo.Instance
Game.IdGenerator        = IdGenerator.Instance
---时间定时器组件
Game.TimerManager       = TimerManager.Instance
---资源管理器
Game.AssetManager       = AssetManager.Instance
---对象池
Game.ObjectPool         = ObjectPool.Instance
---UI管理器
Game.UIManager          = UIManager.Instance
---Scene场景管理器
Game.SceneManager       = SceneManager.Instance

function Game:GameStart()
    Game.TimerManager:OnStart()
    Game.UIManager:OnStart()
    ---@private
    Game.container = EntityHelper.Instance:CreateEntity(Entity)
    ---@type UpdateCenter
    Game.UpdateCenter = Game.container:AddComponent(UpdateCenter)
    ---@type Messenger 数据驱动中心
    Game.DataEventCenter = Game.container:AddNonEntity(Messenger)
    ---@type Messenger 全局事件中心
    Game.GlobalEventCenter = Game.container:AddNonEntity(Messenger)
end

function Game:OnDestroy()
    Game.container:OnDestroy()
end

return Game