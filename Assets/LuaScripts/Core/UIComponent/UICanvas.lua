
---@class UICanvas
UICanvas = Class("UICanvas", Entity)

---@private
function UICanvas:ctor()
    
end

function UICanvas:OnStart()
    self.unity_canvas = self.gameObject:GetComponent(typeof(CS.UnityEngine.Canvas))
    if not Null(self.unity_canvas) then
        self.transform = self.unity_canvas.transform
    end
end 

function UICanvas:SetOrder(order)
    if not Null(self.unity_canvas) then
        self.unity_canvas.sortingOrder = order
    end
end

function UICanvas:OnEnable()
    
end 

function UICanvas:OnDisable()
    
end 