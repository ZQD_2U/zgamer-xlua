﻿--- comparing with CSharp Coroutine
--- Created by mono
--- *Rely on TimerManager*
---@private
local Coroutine = {
    pool_co         = {},       -- running coroutine cache pool
    pool_action     = {},       -- action pool, assistant of coroutine 
    map_action      = {},
    pool_yield      = {},
    map_yield       = {},
}

---@private
function Coroutine:CreateCoroutine(func, ...)
    local args = SafePack(...)
    while func do
        local ret = SafePack(func(SafeUnPack(args)))
        Coroutine:RecycleCoroutine(coroutine.running())
        args = SafePack(coroutine.yield(SafeUnPack(ret)))
        func = args[1]
        table.remove(args, 1)
    end
end

---@private
function Coroutine:RecycleCoroutine(co)
    if not coroutine.status(co) == "suspended" then
        error("Try to recycle coroutine not suspended : "..coroutine.status(co))
    end
    table.insert(self.pool_co, co)
end

--- coroutine run in safe mode and no any exception thrown, so we need to catch the error intelligence.
--- the co may be destroyed any time and error be caught, but this is normal, so just log the error but not throw error and crash the application.
---@private flag resume the co and run it until next yield.
function Coroutine:Resume(co, func, ...)
    local resume_res = nil
    if func ~= nil then
        resume_res = SafePack(coroutine.resume(co, func, ...))
    else
        resume_res = SafePack(coroutine.resume(co, ...))
    end
    local flag, err = resume_res[1], resume_res[2]
    if not flag then
        Debug.LogError(err .. "\n" .. debug.traceback(co))
    elseif resume_res.n > 1 then
        table.remove(resume_res, 1)
    else
        resume_res = nil
    end
    return flag, resume_res
end

---@private
function Coroutine:GetCoroutine()
    if table.Length(self.pool_co) > 0 then
        return table.remove(self.pool_co)
    else
        return coroutine.create(Bind(Coroutine.CreateCoroutine, Coroutine))
    end
end

---@private
function Coroutine:GetAction(co, func, args, result)
    local action = nil
    if not table.IsNilOrEmpty(self.pool_action) then
        action = table.remove(self.pool_action)
    else
        action = {}
    end
    action.co = co or false
    action.task = false
    action.func = func or false
    action.args = args or false
    action.result = result or false
    return action
end
---@private
function Coroutine:RecycleAction(action)
    action.co = false
    action.task = false
    action.func = false
    action.args = false
    action.result = false
    table.insert(self.pool_action, action)
end

--- check custom coroutine action state
---@private
function Coroutine:ActionCheck(action, abort, ...)
    assert(action.task, "action task is nil.")
    if not action.func then
        abort = true
    end
    if not abort and action.func then
        if action.args and action.args.n > 0 then
            abort = (action.func(SafeUnPack(action.args)) == action.result)
        else
            abort = (action.func() == action.result)
        end
    end
    if not abort then return end
    action.task:Stop()
    self.map_action[action.co] = nil
    self:Resume(action.co, ...)
    self:RecycleAction(action)
end

---@private boolean use to check async task state
---@param co thread
---@generic object
---@param asyncOp any|table|object any object contains isDone and progress field
---@param asyncProgressCb fun(co:thread, progress:number)
---@return boolean
function Coroutine:AsyncOperationCheck(co, asyncOp, asyncProgressCb)
    if asyncProgressCb ~= nil then
        asyncProgressCb(co, asyncOp.progress)
    end
    return asyncOp.isDone
end

---@public void start a new coroutine and run
coroutine.startNew = function(func, ...)
    local co = Coroutine:GetCoroutine()
    Coroutine:Resume(co, func, ...)
    return co
end

---@public void run in coroutine
---@param func function
---@param asyncProgressCb fun(co:thread, progress:number) execute every frame and update progress from 0 to 1
coroutine.yieldStart = function(func, asyncProgressCb, ...)
    
end

--- different with cs coroutine, this will certainly wait 1 frame.
---@public void run in coroutine
coroutine.yieldReturn = function(...)

end

---@public void run in coroutine
---@param frames number
coroutine.waitForFrames = function(frames)
    if type(frames) ~= LuaType.Number then return end
    local co = coroutine.running() or error("coroutine.waitForFrames must be run in coroutine.")
    local action = Coroutine:GetAction(co)
    local task = TimerManager.Instance:DelayCoAction(frames,false, true, Coroutine.ActionCheck, Coroutine, action)
    action.task = task
    Coroutine.map_action[co] = action
    return coroutine.yield()
end

---@public void run in coroutine, wait until the end of this frame
coroutine.waitForEndOfFrame = function()
    local co = coroutine.running() or error("coroutine.waitForEndOfFrame must be run in coroutine.")
    local action = Coroutine:GetAction(co)
    local task = TimerManager.Instance:DelayCoLateAction(0,false, true, Coroutine.ActionCheck, Coroutine, action)
    action.task = task
    Coroutine.map_action[co] = action
    return coroutine.yield()
end

---@public void run in coroutine
---@param seconds number unit is seconds/s
coroutine.waitForSeconds = function(seconds)
    if type(seconds) ~= LuaType.Number then return end
    local co = coroutine.running() or error("coroutine.waitForSeconds must be run in coroutine.")
    local action = Coroutine:GetAction(co)
    local task = TimerManager.Instance:DelayCoAction(seconds,false, false, Coroutine.ActionCheck, Coroutine, action)
    action.task = task
    Coroutine.map_action[co] = action
    return coroutine.yield()
end

---@public void run in coroutine
---@generic isDone
---@generic progress
---@param asyncOperation userdata handle to an asynchronous, any contains field of "isDone" and "progress" object (lua or CSharp)
---@param asyncProgressCb fun(co:thread, progress:number) execute every frame and update progress from 0 to 1
coroutine.waitAsyncOperation = function(asyncOperation, asyncProgressCb)
    assert(asyncOperation, "asyncOperation is nil.")
    local co = coroutine.running() or error ("coroutine.waitAsyncOperation must be run in coroutine")
    local action = Coroutine:GetAction(co, Bind(Coroutine.AsyncOperationCheck, Coroutine), SafePack(co, asyncOperation, asyncProgressCb), true)
    local task = TimerManager.Instance:DelayCoAction(1,true, true, Coroutine.ActionCheck, Coroutine, action)
    action.task = task
    return coroutine.yield()
end

---@public void stop coroutine wait operation (except asyncOperation)
---@param co thread assigned coroutine
coroutine.stopWaiting = function(co, ...)
    local action = Coroutine.map_action[co]
    if action == nil then return end
    Coroutine:ActionCheck(action, true, ...)
end