﻿--- async task. created by mono.

---@class Task
Task = Class("Task")

---@private tb task state
TaskState = {
    Waiting     = 1,
    Running     = 2,
    Suspend     = 3,
}

--- the constructor
---@private
function Task:ctor()
    self.target         = {}
    self.target.func    = nil   -- callback of target
    self.target.args    = nil   -- args of target(usually is "self" as callback first args)
    self.during         = 0     -- delay time, frames or seconds
    self.cur            = 0     -- has executed time 
    self.loop           = false -- true then loop execute task
    self.frame_task     = true  -- true is frames else is seconds
    self.unscaled       = false -- if true then use unscaled dealtTime else normal dealtTime
    self.state          = TaskState.Waiting -- state of task
end

---@public Task start task
---@param during number the duration of the task, frames or seconds according to isFrame
---@param loop boolean if true then execute task looping else only one time
---@param isFrame boolean if true then update by frames else seconds
---@param unscaled boolean if ture then task will updating and not influenced by Time.scale
---@generic object
---@param finishedCb fun(instance:object, ...)
---@param instance object first args of finishedCb
function Task:Start(during, loop, isFrame, unscaled, finishedCb, instance, ...)
    if finishedCb == nil then return end
    self.target.func = finishedCb
    self.target.args = SafePack(instance, ...)
    self.during = during
    self.cur = isFrame and -1 or 0
    self.loop = loop
    self.frame_task = isFrame
    self.unscaled = unscaled
    self.state = TaskState.Running
end

---@protected void
---@param isFixedDealtTime boolean if true then update with fixedDealtTime else dealtTime
function Task:Update(isFixedDealtTime)
    if self.state ~= TaskState.Running then return end
    if self.frame_task then
        self.cur = self.cur + 1
    else
        if isFixedDealtTime then
            --- use fixed step length update task.
            self.cur = self.cur + Time.fixedDeltaTime
        else
            self.cur = self.cur + (self.unscaled and Time.unscaledDeltaTime or Time.deltaTime)
        end
    end
    if self.cur < self.during then return end
    if self.loop then
        self.cur = 0
    else
        self.state = TaskState.Waiting
    end
    local status, err = pcall(self.target.func, SafeUnPack(self.target.args))
    if not status then
        Debug.LogError(err)
        self.state = TaskState.Waiting
    end
end

---@public void pause task, call task.Resume restore it. used with resume
---@see Task function Resume
function Task:Pause()
    self.state = TaskState.Suspend
end

---@public void restore task, used with pause.
function Task:Resume()
    self.state = TaskState.Running
end

---@public void reset task run time
function Task:Reset()
    self.cur = 0
end

---@public void stop task and destroyed.
function Task:Stop()
    self.state = TaskState.Waiting
    self.during = 0
    self.target.func = nil
    self.target.args = nil
end

function Task:IsDead()
    return self.state == TaskState.Waiting or self.target.func == nil
end
