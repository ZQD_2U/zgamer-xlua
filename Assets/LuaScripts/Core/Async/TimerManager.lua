﻿
---@class TimerManager
TimerManager = Class("TimerManager", Singleton)

---@private
function TimerManager:ctor()
    self._update_handle = nil
    self._lateUpdate_handle = nil
    self._fixedUpdate_handle = nil

    self._coUpdate_handle = nil
    self._coLateUpdate_handle = nil
    self._coFixedUpdate_handle = nil
    
    self.update_tasks           = LinkedList:new()
    self.lateUpdate_tasks       = LinkedList:new()
    self.fixedUpdate_tasks      = LinkedList:new()

    self.coUpdate_tasks         = LinkedList:new()
    self.coLateUpdate_tasks     = LinkedList:new()
    self.coFixedUpdate_tasks    = LinkedList:new()
    
    self.cache_tasks   = {}
end

function TimerManager:OnStart()
    self:OnDispose()
    self._update_handle         = UpdateBeat:AddListener(self.UpdateHandle, self)
    self._lateUpdate_handle     = LateUpdateBeat:AddListener(self.LateUpdateHandle, self)
    self._fixedUpdate_handle    = FixedUpdateBeat:AddListener(self.FixedUpdateHandle, self)

    self._coUpdate_handle       = CoUpdateBeat:AddListener(self.CoUpdateHandle, self)
    self._coLateUpdate_handle   = CoLateUpdateBeat:AddListener(self.CoLateUpdateHandle, self)
    self._coFixedUpdate_handle  = CoFixedUpdateBeat:AddListener(self.CoFixedUpdateHandle, self)
end

---@private void
---@param list LinkedList
function TimerManager:Recycle(list)
    for n, v in ipLkList(list) do
        if v:IsDead() then
            v:Stop()
            table.insert(self.cache_tasks, v)
            list:RemoveNode(n)
        end
    end
end
---@return Task
function TimerManager:GetOrCreateTask()
    if table.IsNilOrEmpty(self.cache_task) then
        return Task.new()
    else
        return table.remove(self.cache_task)
    end
end
---@private
function TimerManager:UpdateHandle()
    if self.update_tasks:Count() == 0 then return end
    for _, v in ipLkList(self.update_tasks) do
        v:Update(false)
    end
    self:Recycle(self.update_tasks)
end
---@private
function TimerManager:LateUpdateHandle()
    if self.lateUpdate_tasks:Count() == 0 then return end
    for _, v in ipLkList(self.lateUpdate_tasks) do
        v:Update(false)
    end
    self:Recycle(self.lateUpdate_tasks)
end
---@private
function TimerManager:FixedUpdateHandle()
    if self.fixedUpdate_tasks:Count() == 0 then return end
    for _, v in ipLkList(self.fixedUpdate_tasks) do
        v:Update(true)
    end
    self:Recycle(self.fixedUpdate_tasks)
end
---@private
function TimerManager:CoUpdateHandle()
    if self.coUpdate_tasks:Count() == 0 then return end
    for _, v in ipLkList(self.coUpdate_tasks) do
        v:Update(false)
    end
    self:Recycle(self.coUpdate_tasks)
end
---@private
function TimerManager:CoLateUpdateHandle()
    if self.coLateUpdate_tasks:Count() == 0 then return end
    for _, v in ipLkList(self.coLateUpdate_tasks) do
        v:Update(false)
    end
    self:Recycle(self.coLateUpdate_tasks)
end
---@private
function TimerManager:CoFixedUpdateHandle()
    if self.coFixedUpdate_tasks:Count() == 0 then return end
    for _, v in ipLkList(self.coFixedUpdate_tasks) do
        v:Update(true)
    end
    self:Recycle(self.coFixedUpdate_tasks)
end

---通用延迟执行方法
---@param duration number 持续时间, 单位 /s 秒
---@param loop boolean 是否循环执行
---@generic object
---@param finishedCb fun(instance:object, ...) 等待结束执行回调
---@param instance object 回调参数
---@return Task
function TimerManager:DelayAction(duration, loop, finishedCb, instance, ...)
    local task = self:GetOrCreateTask()
    task:Start(duration, loop,false,false, finishedCb, instance, ...)
    self.update_tasks:Add(task)
    return task
end

---通用延迟执行方法
---@param frames number 持续帧数, 单位帧
---@param loop boolean 是否循环执行
---@generic object
---@param finishedCb fun(instance:object, ...) 等待结束执行回调
---@param instance object 回调参数
---@return Task
function TimerManager:DelayFrameAction(frames, loop, finishedCb, instance, ...)
    local task = self:GetOrCreateTask()
    task:Start(frames, loop,true,false, finishedCb, instance, ...)
    self.update_tasks:Add(task)
    return task
end

---固定步长延迟执行方法
---@param seconds number 持续时间, 秒
---@param loop boolean 是否循环执行
---@generic object
---@param finishedCb fun(instance:object, ...) 等待结束执行回调
---@param instance object 回调参数
---@return Task
function TimerManager:DelayFixedAction(seconds, loop, finishedCb, instance, ...)
    local task = self:GetOrCreateTask()
    task:Start(seconds, loop,false,false, finishedCb, instance, ...)
    self.fixedUpdate_tasks:Add(task)
    return task
end

---固定步长延迟执行方法
---@param frames number 持续时间, 帧
---@param loop boolean 是否循环执行
---@generic object
---@param finishedCb fun(instance:object, ...) 等待结束执行回调
---@param instance object 回调参数
---@return Task
function TimerManager:DelayFixedFrameAction(frames, loop, finishedCb, instance, ...)
    local task = self:GetOrCreateTask()
    task:Start(frames, loop,true,false, finishedCb, instance, ...)
    self.fixedUpdate_tasks:Add(task)
    return task
end

---协程延迟任务 Update
---@param duration number 持续时间, 帧或者秒
---@param loop boolean 是否循环执行
---@param isFrame boolean 是否循环执行
---@generic object
---@param finishedCb fun(instance:object, task:Task, ...) 等待结束执行回调
---@param instance object 回调参数 一般是 self 作为第一个参数传递给回调本身
---@return Task
function TimerManager:DelayCoAction(duration, loop, isFrame, finishedCb, instance, ...)
    local task = self:GetOrCreateTask()
    --- 这里 task 作为 第二个参数 传递给 coroutine 的状态检查，方便关闭任务
    task:Start(duration, loop, isFrame,false, finishedCb, instance, ...)
    self.coUpdate_tasks:Add(task)
    return task
end

---协程延迟任务 LateUpdate
---@param duration number 持续时间, 帧或者秒
---@param loop boolean 是否循环执行
---@param isFrame boolean 是否循环执行
---@generic object
---@param finishedCb fun(instance:object, ...) 等待结束执行回调
---@param instance object 回调参数 一般是 self 作为第一个参数传递给回调本身
---@return Task
function TimerManager:DelayCoLateAction(duration, loop, isFrame, finishedCb, instance, ...)
    local task = self:GetOrCreateTask()
    --- 这里 task 作为 第二个参数 传递给 coroutine 的状态检查，方便关闭任务
    task:Start(duration, loop, isFrame,false, finishedCb, instance, ...)
    self.coLateUpdate_tasks:Add(task)
    return task
end

function TimerManager:OnDispose()
    local function removeHandle(handle)
        if handle == nil then return end
        UpdateBeat:RemoveListener(handle)
    end
    removeHandle(self._update_handle)
    removeHandle(self._lateUpdate_handle)
    removeHandle(self._fixedUpdate_handle)
    removeHandle(self._coUpdate_handle)
    removeHandle(self._coLateUpdate_handle)
    removeHandle(self._coFixedUpdate_handle)

    self._update_handle = nil
    self._lateUpdate_handle = nil
    self._fixedUpdate_handle = nil

    self._coUpdate_handle = nil
    self._coLateUpdate_handle = nil
    self._coFixedUpdate_handle = nil
end

function TimerManager:OnDestroy()
    self.base:OnDestroy()
    self:OnDispose()
end

---@type TimerManager
TimerManager.Instance = TimerManager:GetInstance()
