﻿
---@class UpdateCenter
UpdateCenter = Class("UpdateCenter", Entity)

---@private
function UpdateCenter:ctor()
    self._update_list = setmetatable({}, { __mode = "k"})
    self._late_update_list = setmetatable({}, { __mode = "k"})
    self._fixed_update_list = setmetatable({}, { __mode = "k"})
end
---@private
function UpdateCenter:OnStart()
    self._update_handle = UpdateBeat:AddListener(self.Update, self)
    self._late_update_handle = LateUpdateBeat:AddListener(self.LateUpdate, self)
    self._fixed_update_handle = FixedUpdateBeat:AddListener(self.FixedUpdate, self)
end
---@private
function UpdateCenter:Update()
    for func, _ in pairs(self._update_list) do func() end
end
---@private
function UpdateCenter:LateUpdate()
    for func, _ in pairs(self._late_update_list) do func() end
end
---@private
function UpdateCenter:FixedUpdate()
    for func, _ in pairs(self._fixed_update_list) do func() end
end

---@public
---@param updater fun()
---@param inst table 通常是self
function UpdateCenter:AddUpdater(updater, inst)
    local handle = Bind(updater, inst)
    self._update_list[handle] = true
    return handle
end

---@public
---@param updater fun()
---@param inst table 通常是self
function UpdateCenter:AddLateUpdater(updater, inst)
    local handle = Bind(updater, inst)
    self._late_update_list[handle] = true
    return handle
end

---@public
---@param updater fun()
---@param inst table 通常是self
function UpdateCenter:AddFixedUpdater(updater, inst)
    local handle = Bind(updater, inst)
    self._fixed_update_list[handle] = true
    return handle
end

---@public
---@generic BindHandle
---@param handle BindHandle 
function UpdateCenter:RemoveUpdater(handle)
    self._update_list[handle] = nil
end

---@public
---@generic BindHandle
---@param handle BindHandle
function UpdateCenter:RemoveLateUpdater(handle)
    self._late_update_list[handle] = nil
end

---@public
---@generic BindHandle
---@param handle BindHandle
function UpdateCenter:RemoveFixedUpdater(handle)
    self._fixed_update_list[handle] = nil
end

---@public
function UpdateCenter:Clear()
    self._update_list = setmetatable({}, { __mode = "k"})
    self._late_update_list = setmetatable({}, { __mode = "k"})
    self._fixed_update_list = setmetatable({}, { __mode = "k"})
end

---@private
function UpdateCenter:OnDestroy()
    if self._update_handle ~= nil then
        UpdateBeat:RemoveListener(self._update_handle)
    end
    if self._late_update_handle ~= nil then
        LateUpdateBeat:RemoveListener(self._late_update_handle)
    end
    if self._fixed_update_handle ~= nil then
        FixedUpdateBeat:RemoveListener(self._fixed_update_handle)
    end
    self._update_list = nil
    self._late_update_list = nil
    self._fixed_update_list = nil
    self._update_handle = nil
    self._late_update_handle = nil
    self._fixed_update_handle = nil
    
    self.base.OnDestroy(self)
end 