
---@class UIManager
UIManager = Class("UIManager", Singleton)

---@private
function UIManager:ctor()
    self.layerDic = Dictionary:new(LuaType.String, LuaType.Table)
    ---@private readonly 分辨率
    self.Resolution = Vector2.New(1920, 1080)
    ---窗口层级间距
    self.MaxOderPerWindow = 10
end

function UIManager:OnStart()
    self.gameObject = NewGameObject("UIRoot")
    self.rectTransform = self.gameObject:AddComponent(typeof(CS.UnityEngine.RectTransform))
    self.transform = self.rectTransform.transform
    DontDestroyOnLoad(self.gameObject)
    
    local eventSystem = NewGameObject("EventSystem")
    eventSystem:AddComponent(typeof(CS.UnityEngine.EventSystems.EventSystem))
    eventSystem:AddComponent(typeof(CS.UnityEngine.EventSystems.StandaloneInputModule))
    DontDestroyOnLoad(eventSystem)
    
    local uiCameraObj = NewGameObject("UICamera")
    uiCameraObj.layer = LayerMask.NameToLayer("UI")
    uiCameraObj:AddComponent(typeof(CS.UnityEngine.RectTransform))
    self.UICamera = uiCameraObj:AddComponent(typeof(CS.UnityEngine.Camera))
    self.UICamera.orthographicSize = self.Resolution.y / 2
    self.UICamera.cullingMask = LayerMask.GetMask("UI")
    self.UICamera.orthographic = true
    self.UICamera.clearFlags = CS.UnityEngine.CameraClearFlags.Nothing
    uiCameraObj.transform:SetParent(self.transform)
    ---设置游戏窗口分辨率
    CS.UnityEngine.Screen.SetResolution(self.Resolution.x, self.Resolution.y, true)
    
    ---UI 层级配置
    require "Core.UI.UILayers"
    local res = UILayers:GetSortedLayers()
    for _, v in ipairs(res) do
        local obj = NewGameObject(v.Name)
        local uiLayer = UILayer.new(obj, v)
        uiLayer.transform:SetParent(self.transform)
        self.layerDic:Add(v.Name, uiLayer)
    end
end

---@public void 打开窗口
---@param winHandle UILayers 窗口配置.
function UIManager:OpenWindow(winHandle, ...)
    assert(winHandle, "window config is nil.")
    ---@type UILayer
    local layer = self.layerDic:GetValue(winHandle.LayerName)
    if layer == nil then
        Debug.LogError(string.format("form layerName is not exist: %s", winHandle.LayerName))
        return
    end
    layer:OpenWindow(winHandle, ...)
end

---@public void 关闭窗口
function UIManager:CloseWindow(winHandle)
    assert(winHandle, "window config is nil.")
    ---@type UILayer
    local layer = self.layerDic:GetValue(winHandle.LayerName)
    if layer == nil then
        Debug.LogError(string.format("form layerName is not exist: %s", winHandle.LayerName))
        return
    end
    layer:CloseWindow(winHandle)
end

---@public void 回收 窗口
---@param keepLayers table 保持不回收的层级
function UIManager:RecycleWindowLayers(keepLayers)
    for k, v in ipDictionary(self.layerDic) do
        if k ~= keepLayers.Name then
            v:RecycleWindows()
        end
    end
end

---@public void 回收指定窗口
function UIManager:RecycleWindow(winHandle)
    assert(winHandle, "window config is nil.")
    ---@type UILayer
    local layer = self.layerDic:GetValue(winHandle.LayerName)
    if layer == nil then
        Debug.LogError(string.format("form layerName is not exist: %s", winHandle.LayerName))
        return
    end
    layer:RecycleWindow(winHandle)
end

---@type UIManager
UIManager.Instance = UIManager:GetInstance()