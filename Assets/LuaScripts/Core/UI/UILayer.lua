﻿--- 层级内 管理自己的 窗口组
---@class UILayer
UILayer = Class("UILayer")

---@private
function UILayer:ctor(obj, layerData)
    self.layerData = layerData
    self.gameObject = obj
    self.transform = self.gameObject.transform

    self.canvas = self.gameObject:AddComponent(typeof(CS.UnityEngine.Canvas))
    self.canvasScaler = self.gameObject:AddComponent(typeof(CS.UnityEngine.UI.CanvasScaler))
    self.gameObject:AddComponent(typeof(CS.UnityEngine.CanvasRenderer))
    self.gameObject:AddComponent(typeof(CS.UnityEngine.UI.GraphicRaycaster))
    self.canvasScaler.uiScaleMode = CS.UnityEngine.UI.CanvasScaler.ScaleMode.ScaleWithScreenSize
    self.canvasScaler.screenMatchMode = CS.UnityEngine.UI.CanvasScaler.ScreenMatchMode.MatchWidthOrHeight
    self.canvasScaler.referenceResolution = UIManager.Instance.Resolution
    self.canvasScaler.matchWidthOrHeight = 1.0
    
    self.gameObject = self.canvas.gameObject
    self.transform = self.gameObject.transform
    self.gameObject.layer = LayerMask.NameToLayer("UI")

    self.canvas.sortingLayerName = "UI"
    self.canvas.renderMode = CS.UnityEngine.RenderMode.ScreenSpaceCamera
    self.canvas.worldCamera = UIManager.Instance.UICamera
    self.canvas.sortingOrder = self.layerData.OrderInLayer
    self.canvas.planeDistance = self.layerData.Distance

    ---窗口缓存
    self._cache_form = Dictionary:new(LuaType.String, LuaType.Table)
    ---管理当前 层级内窗口栈
    self.popStack = Stack:new()
end

---@private
function UILayer:GetNewForm(winHandle, args)
    local obj = AssetManager.Instance:AwaitLoadAsset(winHandle.PrefabPath, typeof(CS.UnityEngine.GameObject))
    local instance = CS.UnityEngine.Object.Instantiate(obj, self.transform)
    local form = EntityHelper.Instance:CreateEntity(UIWinForm, instance, SafeUnPack(args))
    form:OnStart(winHandle, SafeUnPack(args))
    return form
end

---@public void OpenWindow
---@param winHandle table
function UILayer:OpenWindow(winHandle, ...)
    local args = SafePack(...)
    coroutine.startNew(function()
        ---@type UIWinForm|Entity
        local form = self._cache_form:GetValue(winHandle.Name)
        if form == nil then
            form = self:GetNewForm(winHandle, args)
        else
            self._cache_form:Remove(winHandle.Name)
        end
        self.popStack:Push(form)
        self:ResetOrder()
        form:OnEnable(SafeUnPack(args))
    end)
end 

---@protected void 关闭窗口 内部调用
function UILayer:CloseWindow(winHandle)
    local form = self.popStack:Pop()
    form:OnDisable()
    self._cache_form:Add(winHandle.Name, form)
end 

---@private void 更新层级
function UILayer:ResetOrder()
    local minOrder = self.layerData.OrderInLayer
    local curOrder = minOrder + UIManager.Instance.MaxOderPerWindow
    for _, v in ipStack(self.popStack) do
        v:ResetOrder(curOrder)
        curOrder = curOrder + UIManager.Instance.MaxOderPerWindow
    end
end

---@internal void 回收 窗口
function UILayer:RecycleWindows()
    local form = self.popStack:Pop()
    while form ~= nil do
        form:OnDisable()
        form:OnDestroy()
        CS.UnityEngine.GameObject.Destroy(form)
        form = self.popStack:Pop()
    end
    for _, v in ipDictionary(self._cache_form) do
        v:OnDestroy()
    end
    self._cache_form:Clear()
end

---@private void 回收指定窗口
function UILayer:RecycleWindow(winHandle)
    local form = self._cache_form:GetValue(winHandle.Name)
    form:OnDestroy()
    self._cache_form:Remove(winHandle.Name)
end 