﻿
---@class UIWinForm 窗口实体
UIWinForm = Class("UIWinForm", Entity)

function UIWinForm:OnStart(winHandle, ...)
    local binder, model, ctrl, view
    if winHandle.Binder ~= nil then
        binder = EntityHelper.Instance:CreateEntity(winHandle.Binder, self.gameObject)
        self:AddEntity(binder)
        self.binder = binder
        binder:OnStart()
    end
    if winHandle.Model ~= nil then
        model = EntityHelper.Instance:CreateEntity(winHandle.Model)
        self:AddEntity(model)
        self.model = model
        model:OnStart(...)
    end
    if winHandle.Ctrl ~= nil then
        ctrl = EntityHelper.Instance:CreateEntity(winHandle.Ctrl)
        ctrl.model = model
        self:AddEntity(ctrl)
        self.ctrl = ctrl
    end
    if winHandle.View ~= nil then
        view = EntityHelper.Instance:CreateEntity(winHandle.View, self.gameObject)
        view:AddComponent(UICanvas)
        view.binder = binder
        view.model = model
        view.ctrl = ctrl
        self:AddEntity(view)
        self.view = view
        view:OnStart(...)
    end
end

function UIWinForm:OnEnable(...)
    self.base.OnEnable(self)
    if self.view ~= nil then self.view:OnEnable(...) end
    if self.model ~= nil then self.model:OnEnable(...) end
end 

---@private void 重置窗口 层级
function UIWinForm:ResetOrder(order)
    if self.view == nil then return end
    self.view:GetComponent(UICanvas):SetOrder(order)
end

function UIWinForm:OnDisable()
    if self.view ~= nil then self.view:OnDisable() end
    if self.model ~= nil then self.model:OnDisable() end
    self.base.OnDisable(self)
end 

function UIWinForm:OnDestroy()
    if self.view ~= nil then self.view:OnDestroy() end
    if self.model ~= nil then self.model:OnDestroy() end
    self.base.OnDestroy(self)
end 