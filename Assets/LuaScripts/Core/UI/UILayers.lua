﻿
---@class UILayers
UILayers = {}

UILayers.SceneLayer      = { Name = "SceneLayer",       Distance = 1000, OrderInLayer = 0 }
UILayers.BackGroundLayer = { Name = "BackGroundLayer",  Distance = 900,  OrderInLayer = 1000 }
UILayers.NormalLayer     = { Name = "NormalLayer",      Distance = 800,  OrderInLayer = 2000 }
UILayers.InfoLayer       = { Name = "InfoLayer",        Distance = 700,  OrderInLayer = 3000 }
UILayers.TipLayer        = { Name = "TipLayer",         Distance = 600,  OrderInLayer = 4000 }
UILayers.TopLayer        = { Name = "TopLayer",         Distance = 500,  OrderInLayer = 5000 }

function UILayers:GetSortedLayers()
    local res = {}
    for _, v in pairs(UILayers) do
        if type(v) == LuaType.Table then
            res[#res + 1] = v 
        end
    end
    table.sort(res, function(a, b) return a.Distance > b.Distance end)
    return res
end

setmetatable(UILayers, {__newindex = function() Debug.LogWarning("Don't insert new field into UILayers! it's readonly.") end})