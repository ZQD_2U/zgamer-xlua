﻿
---@class Messenger
Messenger = Class("Messenger", Entity)

---@private
function Messenger:ctor()
    self._events = {}
end

---@public void 添加监听
---@param eventID number|string 事件ID
---@param func fun(...:table)
function Messenger:AddListener(eventID, func, ...)
    local es = self._events[eventID]
    if es == nil then es = setmetatable({}, {__mode = "k"}) end
    if es[func] ~= nil then
        Debug.LogWarning(string.format("you're already add listener, eventID: %d", eventID))
        return
    end
    local args = SafePack(...)
    es[func] = args
    self._events[eventID] = es
end

---@public void 添加并覆盖 原有监听
function Messenger:ReAddListener(eventID, func, ...)
    local es = self._events[eventID]
    if es == nil then es = setmetatable({}, {__mode = "k"}) end
    local args = SafePack(...)
    es[func] = args
    self._events[eventID] = es
end

---@public void 移除监听
function Messenger:RemoveListener(eventID, func)
    local es = self._events[eventID]
    if es == nil then return end 
    es[func] = nil
end

---@public void 添加id所有监听
function Messenger:RemoveListeners(eventID)
    self._events[eventID] = nil
end

---@public void 执行监听
function Messenger:Invoke(eventID, ...)
    local es = self._events[eventID]
    for func, args in pairs(es) do
        local params = ConcatSafePack(args, SafePack(...))
        func(SafeUnPack(params))
    end
end

---@public void 清理所有 监听
function Messenger:OnClear()
    self._events = {}
end

function Messenger:OnDestroy()
    self._events = {}
end 