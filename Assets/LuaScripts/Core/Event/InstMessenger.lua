﻿
---@class InstMessenger 
InstMessenger = Class("InstMessenger", Entity)

---@private
function InstMessenger:ctor()
    self._events = {}
end

function InstMessenger:AddListener(eventID, func, inst, ...)
    local es = self._events[eventID]
    if es == nil then es = setmetatable({}, {__mode = "k"}) end
    if es[inst] == nil then es[inst] = setmetatable({}, {__mode = "k"}) end
    if es[inst][func] ~= nil then
        Debug.LogWarning(string.format("you're already add listener, eventID: %d", eventID))
        return
    end
    local args = SafePack(...)
    es[inst][func] = args
    self._events[eventID] = es
end

function InstMessenger:ReAddListener(eventID, func, inst, ...)
    local es = self._events[eventID]
    if es == nil then es = setmetatable({}, {__mode = "k"}) end
    if es[inst] == nil then es[inst] = setmetatable({}, {__mode = "k"}) end
    local args = SafePack(...)
    es[inst][func] = args
    self._events[eventID] = es
end

function InstMessenger:RemoveListener(eventID, func, inst)
    local es = self._events[eventID]
    if es == nil then return end
    if es[inst] == nil then return end
    es[inst][func] = nil
end

function InstMessenger:RemoveListeners(eventID, inst)
    if self._events[eventID] == nil then return end
    self._events[eventID][inst] = nil
end

function InstMessenger:Invoke(eventID, ...)
    local es = self._events[eventID]
    for inst, tb in pairs(es) do
        for func, args in ipairs(tb) do
            local params = ConcatSafePack(args, SafePack(...))
            func(inst, SafeUnPack(params))
        end
    end
end

function InstMessenger:OnClear()
    self._events = {}
end

function InstMessenger:OnDestroy()
    self._events = {}
end