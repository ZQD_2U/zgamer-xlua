﻿
Scenes = {}
Scenes.Loading      = { 
                        SceneName = "Loading",
                        ScenePath = "Assets/ZGamer/BundleResource/Scenes/Loading.unity",
                        Scene = nil
                        }
Scenes.Home         = {
                        SceneName = "Home",
                        ScenePath = "Assets/ZGamer/BundleResource/Scenes/Home.unity",
                        Scene = require "Game.Scene.HomeScene"
                        }
Scenes.Battle       = {}
Scenes.Explore      = {}

setmetatable(Scenes, {__newindex = function() Debug.LogWarning("Don't insert new field into Scenes! it's readonly.") end})