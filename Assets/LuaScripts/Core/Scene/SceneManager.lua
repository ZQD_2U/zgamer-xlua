﻿
---@class SceneManager
SceneManager = Class("SceneManager", Singleton)

---@private
function SceneManager:ctor()
    self.current = nil
    self.loading = false
end

---@public
---@param sceneHandle table 场景配置
---@param loadingForm table 过渡加载界面 如果为 nil 则使用 默认 过渡界面
function SceneManager:SwitchScene(sceneHandle, loadingForm)
    if self.loading then return end
    if self.current ~= nil then
        self.current:OnDestroy()
    end
    self.loading = true
    coroutine.startNew(self.SwitchSceneInternal, self, sceneHandle, loadingForm)
end

---@private
function SceneManager:SwitchSceneInternal(sceneHandle, loadingForm)
    local schedule = {progress = 0.0, desc = ""}
    local curLoadingForm = loadingForm
    if loadingForm == nil then
        UIManager.Instance:OpenWindow(UIWindows.UILoadingForm, schedule)
        curLoadingForm = UIWindows.UILoadingForm
    else
        UIManager.Instance:OpenWindow(loadingForm, schedule)
    end
    schedule.progress = 0.01
    AssetManager.Instance:AwaitLoadScene(Scenes.Loading.ScenePath, nil, false)
    schedule.desc = "正在清理缓存..."
    UIManager.Instance:RecycleWindowLayers(UILayers.TopLayer)
    coroutine.waitForFrames(1)
    schedule.progress = 0.05
    ObjectPool.Instance:DestroyAll()
    coroutine.waitForFrames(1)
    schedule.progress = 0.1
    AssetManager.Instance:Clear()
    coroutine.waitForFrames(1)
    -- GC
    collectgarbage("collect")
    CS.System.GC.Collect()
    schedule.progress = 0.2
    coroutine.waitForFrames(1)
    
    local curProgress = schedule.progress
    schedule.desc = "正在加载场景..."
    AssetManager.Instance:AwaitLoadScene(sceneHandle.ScenePath, function(co, progress)
        schedule.progress = curProgress + progress * 0.2
    end)
    schedule.progress = 0.4
    local scene = EntityHelper.Instance:CreateEntity(sceneHandle.Scene)
    curProgress = schedule.progress
    scene:OnStart(function(progress) schedule.progress = curProgress + progress * 0.6 end)
    schedule.progress = 1
    coroutine.waitForFrames(1)
    UIManager.Instance:CloseWindow(curLoadingForm)
    UIManager.Instance:RecycleWindow(curLoadingForm)
    self.current = scene
    self.loading = false
end

---@type SceneManager
SceneManager.Instance = SceneManager:GetInstance()