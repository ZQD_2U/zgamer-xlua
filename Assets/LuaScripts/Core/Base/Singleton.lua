﻿
---@class Singleton
Singleton = Class("Singleton")

function Singleton:ctor()
    rawset(self.__class_type, "Instance", self)
    self.__class_type.new = nil
    self.__class_type.base.new = nil
end

function Singleton:__delete()
    rawset(self.__class_type, "Instance",nil)
end

function Singleton:GetInstance()
    if rawget(self,"Instance") == nil then
        rawset(self, "Instance", self.new())
    end
    return self.Instance
end

function Singleton:Delete()
    self.Instance = nil
end

return Singleton