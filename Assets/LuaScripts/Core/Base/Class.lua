﻿-----------------------------------------------------------------
---------------------------Create by Mono------------------------
---                         OOP面向对象                        ---
-----------------------------------------------------------------

--- 保存 类 虚表
local class_tb = {}

---@public Class 新建类方法
---@param classname string 类名
---@param super table 父类
Class = function(classname, super)
    assert(type(classname) == LuaType.String and #classname > 0)
    
    --- 创建类类型
    local class_type        = {}
    class_type.base         = super
    class_type.ctor         = false
    class_type.__delete     = false
    class_type.__cname      = classname
    class_type.__cType      = ClassType.class
    
    
    local vb                = {}
    setmetatable(class_type, { __newindex = function(t, k, v) vb[k] = v end, __index = vb })
    
    if type(super) == LuaType.Table and super.__cType == ClassType.class then
        setmetatable(vb, {__index = function(t, k) return class_tb[super][k] end})
    end
    class_tb[class_type] = vb

    ---@public new 创建类实例
    class_type.new = function(...)
        local instance = {}
        instance.__class_type   = class_type
        instance.__cType        = ClassType.instance
        setmetatable(instance, {__index = class_tb[class_type]})

        do local create = nil
        create = function(c, ...)
            if c.base then create(c.base, ...) end
            if c.ctor then c.ctor(instance, ...) end
        end
        create(class_type, ...) end
        instance.base = class_type.base
        ---@public Delete 实例注销 
        instance.Delete = function(self)
            local cur_super = self.__class_type
            while cur_super ~= nil do
                if cur_super.__delete then cur_super.__delete(self) end
                cur_super = cur_super.base
            end
        end
        
        return instance
    end
    return class_type
end
