﻿
---@class TimeInfo
TimeInfo = Class("TimeInfo", Singleton)

function TimeInfo:ctor(...)
    self.dt1970 = CS.CSharpObjectCreator.CreateDateTime(1970, 1, 1, 0, 0, 0, 0)
end

---@return number 
function TimeInfo:ClientNow()
    return (CS.System.DateTime.Now.Ticks - self.dt1970.Ticks) / 10000
end

---@type TimeInfo
TimeInfo.Instance = TimeInfo:GetInstance()