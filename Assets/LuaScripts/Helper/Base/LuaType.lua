﻿---@public LuaType Lua 基本类型
LuaType = {
    ---@type table 表
    Table       = "table",
    ---@type function 方法
    Function    = "function",
    ---@type string 字符串
    String      = "string",
    ---@type number 数字
    Number      = "number",
    ---@type boolean Bool
    Bool        = "Boolean",
    ---@type userdata 用户自定义类型
    UserData    = "userdata",
}

---@public ClassType 面向对象基本类型
ClassType = {
    ---@public class 类类型
    class       = 1,
    ---@public instance 实例类型
    instance    = 2,
}