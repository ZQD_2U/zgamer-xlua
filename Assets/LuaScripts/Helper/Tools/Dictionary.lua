---------------------------------------------------------------
---                     Create by Mono                      ---
---                       Dictionary                        ---
---------------------------------------------------------------

---@class Dictionary
Dictionary = {}
---@private
Dictionary.__index = Dictionary

---@public void new
---@param kType any dictionary key type
---@param vType any dictionary value type
---@return Dictionary
function Dictionary:new(kType, vType)
    local tb = {D = {}, kt = kType, vt = vType}
    return setmetatable(tb, Dictionary)
end

---@public void add value
---@param key any
---@param value any
function Dictionary:Add(key, value)
    assert(type(key) == self.kt or self.kt == nil, 
            string.format("Dictionary limited key type is: %s, but you're trying to push type: %s.", self.kt, type(key)))
    assert(type(value) == self.vt or self.vt == nil,
            string.format("Dictionary limited value type is: %s, but you're trying to push type: %s.", self.vt, type(value)))
    self.D[key] = value
end

---@public void remove value by key
---@param key any
function Dictionary:Remove(key)
    self.D[key] = nil
end

---@public void remove value
---@param value any
function Dictionary:RemoveValue(value)
    for k, v in pairs(self.D) do
        if v == value then self.D[k] = nil end
    end
end

---@public boolean is dictionary contains key
---@param key any
---@return boolean
function Dictionary:ContainsKey(key)
    return self.D[key] ~= nil
end

---@public boolean is dictionary contains value
---@param value any
---@return boolean
function Dictionary:ContainsValue(value)
    for _, v in pairs(self.D) do
        if v == value then return true end
    end
    return false
end

---@public any get value
---@param key any
---@return any value
function Dictionary:GetValue(key)
    return self.D[key]
end

---@public table get all keys
---@return table return all keys
function Dictionary:Keys()
    local keys = {}
    local ids = 1
    for k, _ in pairs(self.D) do
        keys[ids] = k
        ids = ids + 1
    end
    return keys
end

---@public table get all Values
---@return table return all Values
function Dictionary:Values()
    local values = {}
    local ids = 1
    for _, v in pairs(self.D) do
        values[ids] = v
        ids = ids + 1
    end
    return values
end

---@public number count of dictionary
---@return number count
function Dictionary:Count()
    local count = 0
    for _, _ in pairs(self.D) do
        count = count + 1
    end
    return count
end

---@public void clear Stack
function Dictionary:Clear()
    self.D = {}
end

---@private Next used by iterator
function Dictionary:Next()
    self.ipIndex = self.ipIndex + 1
    if self.ipIndex == 0 then
        self.ipIndex = nil
        self.keys = nil
        return nil
    end
    local key = self.keys[self.ipIndex]
    return self.keys[self.ipIndex], self.D[key]
end

---@public foreach iterator for dictionary
---@generic V
---@param D Dictionary<number, V>
---@return fun(tbl: table<number, V>):number, V
_G.ipDictionary = function(D)
    D.keys = D:Keys()
    D.ipIndex = 0
    return function() return D:Next() end
end

--- you can new one stack with Stack:new() or Stack()
setmetatable(Dictionary, {__call = Dictionary.new})
return Dictionary