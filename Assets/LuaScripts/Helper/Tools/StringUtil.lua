﻿
---@public boolean compare two string according to ASCII
---@param str1 string
---@param str2 string
---@return boolean
string.compare = function(str1, str2)
    local l1 = #str1
    local l2 = #str2
    for i = 1, math.min(l1, l2) do
        local asc1 = string.byte(str1, i)
        local asc2 = string.byte(str2, i)
        if asc1 ~= asc2 then
            return asc1 < asc2
        end
    end
    return l1 <= l2
end