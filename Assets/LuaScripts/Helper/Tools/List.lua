﻿---------------------------------------------------------------
---                     Create by Mono                      ---
---                          List                           ---
---------------------------------------------------------------

---@class List
List = {}
---@private
List.__index = List

---@public void list:new("any")
---@param type any list value type
---@return List
function List:new(type)
    local tb = {L = {}, Length = 0, Type = type}
    return setmetatable(tb, List)
end

---@public void Add value
---@param value any
function List:Add(value)
    if type(value) ~= self.Type and self.Type ~= nil then
        Debug.LogError(string.format("List limited type is: %s, but you're trying to add type: %s.", self.Type, type(value)))
        return
    end
    self.Length = self.Length + 1
    self.L[self.Length] = value
end

---@public void insert value to list at index
---@param index number
---@param value any
function List:Insert(index, value)
    if index > self.Length or index < 1 then
        Debug.LogError("Index is out of range.")
        return
    end
    local ids = self.Length
    while ids >= index do
        self.L[ids + 1] = self.L[ids]
        ids = ids - 1
    end
    self.L[index] = value
    self.Length = self.Length + 1
end

---@public void remove list value
---@param value any removed value
function List:Remove(value)
    local ids = self:LastIndexOf(value)
    if ids <= 0 then return end
    self:RemoveAt(ids)
end

---@public void remove all same value in list
---@param value any removed value
function List:RemoveAll(value)
    for i, v in ipairs(self.L) do
        if v == value then 
            self.L[i] = nil
            self.Length = math.max(0, self.Length - 1)
        end
    end
    local ids, trueIds = 1, 1
    while trueIds <= self.Length do
        local v = self.L[ids]
        self.L[ids] = nil
        if v ~= nil then
            self.L[trueIds] = v
            trueIds = trueIds + 1
        end
        ids = ids + 1
    end
end

---@public void remove value by index
---@param index number
function List:RemoveAt(index)
    assert(index >= 1 and index <= self.Length, "index out of range.")
    local value = self.L[index]
    for i = index, self.Length do
        self.L[i] = self.L[i + 1]
    end
    self.L[self.Length] = nil
    self.Length = math.max(0, self.Length - 1)
    return value
end

---@public any get value with index
function List:ValueOfIndex(index)
    assert(index >= 1 and index <= self.Length, "index out of range.")
    return self.L[index]
end

---@public int get value index
---@param value any
---@return number
function List:IndexOf(value)
    for i, v in ipairs(self.L) do
        if v == value then return i end
    end
    return -1
end

---@public int get value last index
---@param value any
---@return number
function List:LastIndexOf(value)
    local ids = self.Length
    while ids > 0 do
        if self.L[ids] == value then return ids end
        ids = ids - 1
    end
    return -1
end

---@public Array list to array
---@return table array[]
function List:ToArray()
    return self.L
end

---@public boolean is list contain value
---@param value any
---@return boolean 
function List:Contains(value)
    for _, v in ipairs(self.L) do
        if v == value then return true end
    end
    return false
end

---@public void sort whole list according to your provided field
---@generic V:any
---@param selector fun(v:V):V return selected comparing field
---@param descending boolean if true then sorting with descending order
function List:OrderBy(selector, descending)
    if self.Length <= 1 then return end
    local compareSingle = function(v1, v2)
        if not descending then
            return v1 > v2 else return v1 < v2
        end
    end
    local compareDouble = function(v1, v2)
        if not descending then
            return v1 <= v2 else return v1 >= v2
        end
    end
    local partition = function(array, low, high)
        local mid = array[low]
        while low < high do
            while low < high and compareSingle(selector(array[high]), selector(mid)) do
                high = high - 1
            end
            array[low] = array[high]
            while low < high and compareDouble(selector(array[low]), selector(mid)) do
                low = low + 1
            end
            array[high] = array[low]
        end
        array[low] = mid
        return low
    end
    local function quickSort(array, left, right)
        if left < right then
            local piv = partition(array, left, right)
            quickSort(array, left, piv - 1)
            quickSort(array, piv + 1, right)
        end
    end
    quickSort(self.L, 1, self.Length)
end

---@public void clear list
function List:Clear()
    for i = 1, self.Length do
        self.L[i] = nil
    end
    self.Length = 0
end

---@private Next used by iterator
function List:Next()
    self.ipIndex = self.ipIndex + 1
    if self.ipIndex > self.Length then
        self.ipIndex = nil
        return nil
    end
    return self.ipIndex, self:ValueOfIndex(self.ipIndex)
end

---@public foreach iterator for List
---@param L List
---@return function
_G.ipList = function(L)
    L.ipIndex = 0
    return function() return L:Next() end
end

--- you can new one list with List:new() or List()
setmetatable(List, {__call = List.new})
return List