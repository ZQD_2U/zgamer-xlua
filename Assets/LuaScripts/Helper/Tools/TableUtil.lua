﻿
---@public number
---@param tb table any table
---@return number return the length of tb
table.Length = function(tb)
    local count = 0
    for _, _ in pairs(tb) do
        count = count + 1
    end
    return count
end

---@public boolean is table nil or no any element
---@param tb table must be array table
---@return boolean
table.IsNilOrEmpty = function(tb)
    if tb == nil then return true end
    return tb[1] == nil
end