﻿---------------------------------------------------------------
---                     Create by Mono                      ---
---                          Stack                          ---
---------------------------------------------------------------

---@class Stack
Stack = {}
---@private
Stack.__index = Stack

---@public void new
---@param type any stack value type
---@return Stack
function Stack:new(type)
    local tb = {Depth = 0, S = {}, Type = type}
    return setmetatable(tb, Stack)
end

---@public void push value to stack
---@param value any
function Stack:Push(value)
    assert(type(value) == self.Type or self.Type == nil,
            string.format("Stack limited type is: %s, but you're trying to push type: %s.", self.Type, type(value)))
    self.Depth = self.Depth + 1
    self.S[self.Depth] = value
end

---@public any pop value
---@return any value
function Stack:Pop()
    if self.Depth == 0 then return nil end
    local v = self.S[self.Depth]
    self.Depth = math.max(0, self.Depth - 1)
    return v
end

---@public any peek top value
function Stack:Peek()
    if self.Depth == 0 then return nil end
    return self.S[self.Depth]
end

---@public void clear Stack
function Stack:Clear()
    self.S = {}
    self.Depth = 0
end

---@private Next used by iterator
function Stack:Next()
    self.ipIndex = self.ipIndex - 1
    if self.ipIndex == 0 then
        self.ipIndex = nil
        return nil
    end
    return self.Depth - self.ipIndex + 1, self.S[self.ipIndex]
end

---@public foreach iterator for Stack
---@generic V
---@param L Stack<number, V>
---@return fun(tbl: table<number, V>):number, V
_G.ipStack = function(L)
    L.ipIndex = L.Depth + 1
    return function() return L:Next() end
end

--- you can new one stack with Stack:new() or Stack()
setmetatable(Stack, {__call = Stack.new})
return Stack