

---avoid primitive pack cut off by nil
---must used with SafeUnPack
---@public
_G.SafePack = function(...)
    local params = {...}
    params.n = select("#", ...)
    return params
end

---@public SafePack concat two safePack and return new one
---@param packL table
---@param packR table
---@return table
_G.ConcatSafePack = function(packL, packR)
    local concat = {}
    for i = 1, packL.n do
        concat[i] = packL[i]
    end
    for i = 1, packR.n do
        concat[i + packL.n] = packR[i]
    end
    concat.n = packL.n + packR.n
    return concat
end

---used with SafePack
_G.SafeUnPack = function(tb)
    return table.unpack(tb, 1, tb.n)
end

---Closure binding. bind function and params, return new function which contains self params
_G.Bind = function(func, ...)
    assert(func ~= nil and type(func) == LuaType.Function, "Bind failure with error: func is nil or not function.")
    local args = SafePack(...)
    return function(...)
        local resArgs = ConcatSafePack(args, SafePack(...))
        return func(SafeUnPack(resArgs))
    end
end

---easy use
---@generic CSObject
---@param obj CSObject
_G.DontDestroyOnLoad = function(obj)
    CS.UnityEngine.Object.DontDestroyOnLoad(obj)
end

---create new CSharp gameObject
---@generic CSObject
---@return CSObject
_G.NewGameObject = function(name)
    return CS.UnityEngine.GameObject(name)
end