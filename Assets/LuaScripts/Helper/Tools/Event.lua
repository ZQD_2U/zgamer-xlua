﻿--- Managed all Lua side update process！Execute by CSharp mono update handle.
--- Create by Mono

---@class Event
Event = {}
---@private
Event.__index = Event

--- execute all registered update function and capture all errors
--- override = equal to listener handles
---@private
local tryCache = {
    __call = function(self, ...)
        local status, err
        if not self.inst then
            status, err = pcall(self.func, ...)
        else
            status, err = pcall(self.func, self.inst, ...)
        end
        if not status then
            error(err .. "\n" .. debug.traceback(), 2)
        end
        return status
    end,
    __eq = function(lh, rh)
        return lh.func == rh.func and lh.inst == rh.inst
    end
}

---@public void register lua update handle 
---@return table
function Event:AddListener(func, inst)
    local handle = setmetatable({func = func, inst = inst}, tryCache)
    if not self.Lock then
        self.Ls:Add(handle)
    else
        table.insert(self.opList, function() self.Ls:Add(handle) end)
    end
    return handle
end

---@public void remove registered update handle
function Event:RemoveListener(handle)
    if handle == nil then return end
    if not self.Lock then
        self.Ls:RemoveAll(handle)
    else
        table.insert(self.opList, function() self.Ls:RemoveAll(handle) end)
    end
end

---@public void clear all registered lua update handle
function Event:Clear()
    self.Ls:Clear()
    self.opList = {}
    self.Lock = false
end

---@private
Event.__call = function(self, ...)
    self.Lock = true
    for n, f in ipLkList(self.Ls) do
        if not f(...) then
            self.Ls:RemoveNode(n)
            self.Lock = false
        end
    end
    local opList = self.opList
    self.Lock = false
    for i, f in ipairs(opList) do
        f()
        opList[i] = nil
    end
end

---@private
---@param name string
---@return Event|tb
local event = function(name)
    ---@class tb
    local tb = {name = name, Lock = false, opList = {}, Ls = LinkedList:new()}
    return setmetatable(tb, Event)
end

---@type Event
UpdateBeat          = event("Update")
LateUpdateBeat      = event("LateUpdate")
FixedUpdateBeat     = event("FixedUpdate")
CoUpdateBeat        = event("CoUpdate")
CoLateUpdateBeat    = event("CoLateUpdate")
CoFixedUpdateBeat   = event("CoFixedUpdate")

---@private
function Update(deltaTime, unscaledDeltaTime)
    Time:SetDeltaTime(deltaTime, unscaledDeltaTime)
    UpdateBeat()
    CoUpdateBeat()
end

---@private
function LateUpdate()
    LateUpdateBeat()
    CoLateUpdateBeat()
    Time:SetFrameCount()
end

---@private
function FixedUpdate(fixedDeltaTime)
    Time:SetFixedDelta(fixedDeltaTime)
    FixedUpdateBeat()
    CoFixedUpdateBeat()
end

---@public void System exist btn
function SystemEscBtn()

end