﻿---------------------------------------------------------------
---                     Create by Mono                      ---
---                       LinkedList                        ---
---------------------------------------------------------------

---@class LinkedList
LinkedList = {}
---@private
LinkedList.__index = LinkedList

---@public void new
---@param type any linkedList value type
---@return LinkedList
function LinkedList:new(type)
    local tb = {Length = 0, _prev = 0, _next = 0, Type = type}
    tb._prev = tb
    tb._next = tb
    return setmetatable(tb, LinkedList)
end

---@public void add value
---@param value any
function LinkedList:Add(value)
    assert(type(value) == self.Type or self.Type == nil,
            string.format("LinkedList limited type is: %s, but you're trying to add type: %s.", self.Type, type(value)))
    local node = {v = value, _prev = 0, _next = 0}
    self._prev._next = node
    node._prev = self._prev
    node._next = self
    ---@private
    self._prev = node
    ---@private
    self.Length = self.Length + 1
end

---@public void remove linkedList value
---@param value any removed value
function LinkedList:Remove(value)
    local node = self:NodeOfValue(value)
    self:RemoveNode(node)
end

---@public void remove linkedList node
---@param node any removed node
function LinkedList:RemoveNode(node)
    if node == nil then return end
    node._next._prev = node._prev
    node._prev._next = node._next
    self.Length = math.max(0, self.Length - 1)
end

---@private void insert node
---@param node table node need to be inserted to linkedList
---@param pivotNode table reference node
---@param pre boolean if true then insert node before pivotNode otherwise insert to it's back
function LinkedList:Insert(node, pivotNode, pre)
    if pre then
        node._prev = pivotNode._prev
        node._next = pivotNode
        node._prev._next = node
        pivotNode._prev = node
    else
        node._prev = pivotNode
        node._next = pivotNode._next
        node._next._prev = node
        pivotNode._next = node
    end
    self.Length = self.Length + 1
end

---@public void remove all same value in list
---@param value any removed value
function LinkedList:RemoveAll(value)
    local nt = self
    repeat
        if nt.v == value then
            nt._next._prev = nt._prev
            nt._prev._next = nt._next
            self.Length = math.max(0, self.Length - 1)
        end
        nt = nt._next
    until nt == self
end

---@private any get node by value
---@param value any
---@return table node
function LinkedList:NodeOfValue(value)
    local nt = self
    repeat
        if nt.v == value then
            return nt
        else
            nt = nt._next
        end
    until nt == self
    return nil
end

---@public void sort whole LinkedList according to your provided field
---@generic V:any
---@param selector fun(v:V):V return selected comparing field
---@param descending boolean if true then sorting with descending order
function LinkedList:OrderBy(selector, descending)
    if self.Length <= 1 then return end
    local compareSingle = function(v1, v2)
        if not descending then
            return v1 > v2 else return v1 < v2
        end
    end
    local compareDouble = function(v1, v2)
        if not descending then
            return v1 <= v2 else return v1 >= v2
        end
    end
    local partition = function(min, max)
        local mid = min
        local tmp = mid.v
        while min ~= max do
            while min ~= max and compareSingle(selector(max.v), tmp) do
                max = max._prev
            end
            min.v = max.v
            while min ~= max and compareDouble(selector(min.v), tmp) do
                min = min._next
            end
            max.v = min.v
        end
        min.v = tmp
        return min
    end
    local function quickSort(min, max)
        if min ~= max then
            local piv = partition(min, max)
            if piv ~= min then
                quickSort(min, piv._prev)
            end
            if piv ~= max then
                quickSort(piv._next, max)
            end
        end
    end
    quickSort(self._next, self._prev)
end

---@public number get linkedList length
---@return number
function LinkedList:Count()
    return self.Length
end

---@public void clear linkedList 
function LinkedList:Clear()
    self._prev  = self
    ---@private
    self._next  = self
    self.Length = 0
end

---@private Next used by iterator
function LinkedList:Next()
    ---@private
    self.current = self.current._next
    if self.current == self then
        self.current = nil
        return nil
    end
    return self.current, self.current.v
end

---@public foreach iterator for LinkedList
---@generic V
---@param L LinkedList<V>
---@return fun(tbl: table<number, V>):number, V
_G.ipLkList = function(L)
    L.current = L
    return function() return L:Next() end
end

--- you can new one linkedList with LinkedList:new() or LinkedList()
setmetatable(LinkedList, {__call = LinkedList.new})
return LinkedList