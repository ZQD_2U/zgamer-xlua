﻿---@public Debug 游戏日志输出模块
Debug = {}
---@private
Debug.stopwatch = nil

---@public Log 普通日志
---@param s string
function Debug.Log(s)
    CS.Debugger.Log(debug.traceback(s, 2))
end

---@public LogWarning 警告日志
---@param s string
function Debug.LogWarning(s)
    CS.Debugger.LogWarning(debug.traceback(s, 2))
end

---@public LogError 错误日志
---@param s string
function Debug.LogError(s)
    CS.Debugger.LogError(debug.traceback(s, 2))
end

---@public void 启动耗时检测
function Debug.StartWatch()
    if Debug.stopwatch == nil then
        Debug.stopwatch = CS.System.Diagnostics.Stopwatch.StartNew()
    else
        Debug.stopwatch:Restart()
    end
end

---@public 当前耗时
---@param msg string
function Debug.Watch(msg)
    if Debug.stopwatch == nil then return end
    Debug.Log(string.format("%s: %d ms.", msg, Debug.stopwatch.ElapsedMilliseconds))
end

---@public void 暂停耗时检测 并输出耗时 毫秒
---@param msg string 输出日志前缀
function Debug.StopWatch(msg)
    if Debug.stopwatch == nil then return end
    Debug.stopwatch:Stop()
    Debug.Log(string.format("%s: %d ms.", msg, Debug.stopwatch.ElapsedMilliseconds))
end

return Debug