﻿require "Global.Global"
GameMain = {}

local function Start()
    Debug.Log("Game start ...")
    Game:GameStart()

    Game.TimerManager:DelayAction(2, false, function()
        Game.SceneManager:SwitchScene(Scenes.Home)
    end)
    Game.DataEventCenter:AddListener(DEventID.PlayerDataChange, function()
        Debug.Log("DataEvent invoke!")
    end)
    
    Game.DataEventCenter:Invoke(DEventID.PlayerDataChange)

    ---启动单元测试
    UnitLauncher:StartUp()
end

local function OnApplicationQuit()
    
end

GameMain.Start = Start
GameMain.OnApplicationQuit = OnApplicationQuit
return GameMain