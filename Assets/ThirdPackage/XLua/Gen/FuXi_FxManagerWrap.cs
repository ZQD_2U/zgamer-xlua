﻿#if USE_UNI_LUA
using LuaAPI = UniLua.Lua;
using RealStatePtr = UniLua.ILuaState;
using LuaCSFunction = UniLua.CSharpFunctionDelegate;
#else
using LuaAPI = XLua.LuaDLL.Lua;
using RealStatePtr = System.IntPtr;
using LuaCSFunction = XLua.LuaDLL.lua_CSFunction;
#endif

using XLua;
using System.Collections.Generic;


namespace XLua.CSObjectWrap
{
    using Utils = XLua.Utils;
    public class FuXiFxManagerWrap 
    {
        public static void __Register(RealStatePtr L)
        {
			ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
			System.Type type = typeof(FuXi.FxManager);
			Utils.BeginObjectRegister(type, L, translator, 0, 0, 0, 0);
			
			
			
			
			
			
			Utils.EndObjectRegister(type, L, translator, null, null,
			    null, null, null);

		    Utils.BeginClassRegister(type, L, __CreateInstance, 10, 1, 1);
			Utils.RegisterFunc(L, Utils.CLS_IDX, "FxLauncherAsync", _m_FxLauncherAsync_xlua_st_);
            Utils.RegisterFunc(L, Utils.CLS_IDX, "FxLauncherAsyncCo", _m_FxLauncherAsyncCo_xlua_st_);
            Utils.RegisterFunc(L, Utils.CLS_IDX, "ResetDownLoadURL", _m_ResetDownLoadURL_xlua_st_);
            Utils.RegisterFunc(L, Utils.CLS_IDX, "FxCheckUpdate", _m_FxCheckUpdate_xlua_st_);
            Utils.RegisterFunc(L, Utils.CLS_IDX, "FxCheckDownloadSize", _m_FxCheckDownloadSize_xlua_st_);
            Utils.RegisterFunc(L, Utils.CLS_IDX, "FxCheckDownload", _m_FxCheckDownload_xlua_st_);
            Utils.RegisterFunc(L, Utils.CLS_IDX, "FxCheckUpdateCo", _m_FxCheckUpdateCo_xlua_st_);
            Utils.RegisterFunc(L, Utils.CLS_IDX, "FxCheckDownloadSizeCo", _m_FxCheckDownloadSizeCo_xlua_st_);
            Utils.RegisterFunc(L, Utils.CLS_IDX, "FxCheckDownloadCo", _m_FxCheckDownloadCo_xlua_st_);
            
			
            
			Utils.RegisterFunc(L, Utils.CLS_GETTER_IDX, "RuntimeMode", _g_get_RuntimeMode);
            
			Utils.RegisterFunc(L, Utils.CLS_SETTER_IDX, "RuntimeMode", _s_set_RuntimeMode);
            
			
			Utils.EndClassRegister(type, L, translator);
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int __CreateInstance(RealStatePtr L)
        {
            return LuaAPI.luaL_error(L, "FuXi.FxManager does not have a constructor!");
        }
        
		
        
		
        
        
        
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_FxLauncherAsync_xlua_st_(RealStatePtr L)
        {
		    try {
            
                ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            
			    int gen_param_count = LuaAPI.lua_gettop(L);
            
                if(gen_param_count == 3&& (LuaAPI.lua_isnil(L, 1) || LuaAPI.lua_type(L, 1) == LuaTypes.LUA_TSTRING)&& (LuaAPI.lua_isnil(L, 2) || LuaAPI.lua_type(L, 2) == LuaTypes.LUA_TSTRING)&& translator.Assignable<FuXi.RuntimeMode>(L, 3)) 
                {
                    string _versionFileName = LuaAPI.lua_tostring(L, 1);
                    string _url = LuaAPI.lua_tostring(L, 2);
                    FuXi.RuntimeMode _runtimeMode;translator.Get(L, 3, out _runtimeMode);
                    
                        var gen_ret = FuXi.FxManager.FxLauncherAsync( _versionFileName, _url, _runtimeMode );
                        translator.Push(L, gen_ret);
                    
                    
                    
                    return 1;
                }
                if(gen_param_count == 2&& (LuaAPI.lua_isnil(L, 1) || LuaAPI.lua_type(L, 1) == LuaTypes.LUA_TSTRING)&& (LuaAPI.lua_isnil(L, 2) || LuaAPI.lua_type(L, 2) == LuaTypes.LUA_TSTRING)) 
                {
                    string _versionFileName = LuaAPI.lua_tostring(L, 1);
                    string _url = LuaAPI.lua_tostring(L, 2);
                    
                        var gen_ret = FuXi.FxManager.FxLauncherAsync( _versionFileName, _url );
                        translator.Push(L, gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + gen_e);
            }
            
            return LuaAPI.luaL_error(L, "invalid arguments to FuXi.FxManager.FxLauncherAsync!");
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_FxLauncherAsyncCo_xlua_st_(RealStatePtr L)
        {
		    try {
            
                ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            
			    int gen_param_count = LuaAPI.lua_gettop(L);
            
                if(gen_param_count == 3&& (LuaAPI.lua_isnil(L, 1) || LuaAPI.lua_type(L, 1) == LuaTypes.LUA_TSTRING)&& (LuaAPI.lua_isnil(L, 2) || LuaAPI.lua_type(L, 2) == LuaTypes.LUA_TSTRING)&& translator.Assignable<FuXi.RuntimeMode>(L, 3)) 
                {
                    string _versionFileName = LuaAPI.lua_tostring(L, 1);
                    string _url = LuaAPI.lua_tostring(L, 2);
                    FuXi.RuntimeMode _runtimeMode;translator.Get(L, 3, out _runtimeMode);
                    
                        var gen_ret = FuXi.FxManager.FxLauncherAsyncCo( _versionFileName, _url, _runtimeMode );
                        translator.Push(L, gen_ret);
                    
                    
                    
                    return 1;
                }
                if(gen_param_count == 2&& (LuaAPI.lua_isnil(L, 1) || LuaAPI.lua_type(L, 1) == LuaTypes.LUA_TSTRING)&& (LuaAPI.lua_isnil(L, 2) || LuaAPI.lua_type(L, 2) == LuaTypes.LUA_TSTRING)) 
                {
                    string _versionFileName = LuaAPI.lua_tostring(L, 1);
                    string _url = LuaAPI.lua_tostring(L, 2);
                    
                        var gen_ret = FuXi.FxManager.FxLauncherAsyncCo( _versionFileName, _url );
                        translator.Push(L, gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + gen_e);
            }
            
            return LuaAPI.luaL_error(L, "invalid arguments to FuXi.FxManager.FxLauncherAsyncCo!");
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_ResetDownLoadURL_xlua_st_(RealStatePtr L)
        {
		    try {
            
            
            
                
                {
                    string _url = LuaAPI.lua_tostring(L, 1);
                    
                    FuXi.FxManager.ResetDownLoadURL( _url );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_FxCheckUpdate_xlua_st_(RealStatePtr L)
        {
		    try {
            
                ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            
			    int gen_param_count = LuaAPI.lua_gettop(L);
            
                if(gen_param_count == 1&& translator.Assignable<System.Action<float>>(L, 1)) 
                {
                    System.Action<float> _checkUpdate = translator.GetDelegate<System.Action<float>>(L, 1);
                    
                        var gen_ret = FuXi.FxManager.FxCheckUpdate( _checkUpdate );
                        translator.Push(L, gen_ret);
                    
                    
                    
                    return 1;
                }
                if(gen_param_count == 0) 
                {
                    
                        var gen_ret = FuXi.FxManager.FxCheckUpdate(  );
                        translator.Push(L, gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + gen_e);
            }
            
            return LuaAPI.luaL_error(L, "invalid arguments to FuXi.FxManager.FxCheckUpdate!");
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_FxCheckDownloadSize_xlua_st_(RealStatePtr L)
        {
		    try {
            
                ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            
			    int gen_param_count = LuaAPI.lua_gettop(L);
            
                if(gen_param_count == 2&& LuaTypes.LUA_TBOOLEAN == LuaAPI.lua_type(L, 1)&& translator.Assignable<System.Action<float>>(L, 2)) 
                {
                    bool _containsPackage = LuaAPI.lua_toboolean(L, 1);
                    System.Action<float> _checkDownload = translator.GetDelegate<System.Action<float>>(L, 2);
                    
                        var gen_ret = FuXi.FxManager.FxCheckDownloadSize( _containsPackage, _checkDownload );
                        translator.Push(L, gen_ret);
                    
                    
                    
                    return 1;
                }
                if(gen_param_count == 1&& LuaTypes.LUA_TBOOLEAN == LuaAPI.lua_type(L, 1)) 
                {
                    bool _containsPackage = LuaAPI.lua_toboolean(L, 1);
                    
                        var gen_ret = FuXi.FxManager.FxCheckDownloadSize( _containsPackage );
                        translator.Push(L, gen_ret);
                    
                    
                    
                    return 1;
                }
                if(gen_param_count == 0) 
                {
                    
                        var gen_ret = FuXi.FxManager.FxCheckDownloadSize(  );
                        translator.Push(L, gen_ret);
                    
                    
                    
                    return 1;
                }
                if(gen_param_count == 2&& translator.Assignable<string[]>(L, 1)&& translator.Assignable<System.Action<float>>(L, 2)) 
                {
                    string[] _packages = (string[])translator.GetObject(L, 1, typeof(string[]));
                    System.Action<float> _checkDownload = translator.GetDelegate<System.Action<float>>(L, 2);
                    
                        var gen_ret = FuXi.FxManager.FxCheckDownloadSize( _packages, _checkDownload );
                        translator.Push(L, gen_ret);
                    
                    
                    
                    return 1;
                }
                if(gen_param_count == 1&& translator.Assignable<string[]>(L, 1)) 
                {
                    string[] _packages = (string[])translator.GetObject(L, 1, typeof(string[]));
                    
                        var gen_ret = FuXi.FxManager.FxCheckDownloadSize( _packages );
                        translator.Push(L, gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + gen_e);
            }
            
            return LuaAPI.luaL_error(L, "invalid arguments to FuXi.FxManager.FxCheckDownloadSize!");
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_FxCheckDownload_xlua_st_(RealStatePtr L)
        {
		    try {
            
                ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            
			    int gen_param_count = LuaAPI.lua_gettop(L);
            
                if(gen_param_count == 2&& translator.Assignable<FuXi.DownloadInfo>(L, 1)&& translator.Assignable<System.Action<float>>(L, 2)) 
                {
                    FuXi.DownloadInfo _downloadInfo;translator.Get(L, 1, out _downloadInfo);
                    System.Action<float> _checkDownload = translator.GetDelegate<System.Action<float>>(L, 2);
                    
                        var gen_ret = FuXi.FxManager.FxCheckDownload( _downloadInfo, _checkDownload );
                        translator.Push(L, gen_ret);
                    
                    
                    
                    return 1;
                }
                if(gen_param_count == 1&& translator.Assignable<FuXi.DownloadInfo>(L, 1)) 
                {
                    FuXi.DownloadInfo _downloadInfo;translator.Get(L, 1, out _downloadInfo);
                    
                        var gen_ret = FuXi.FxManager.FxCheckDownload( _downloadInfo );
                        translator.Push(L, gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + gen_e);
            }
            
            return LuaAPI.luaL_error(L, "invalid arguments to FuXi.FxManager.FxCheckDownload!");
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_FxCheckUpdateCo_xlua_st_(RealStatePtr L)
        {
		    try {
            
                ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            
			    int gen_param_count = LuaAPI.lua_gettop(L);
            
                if(gen_param_count == 1&& translator.Assignable<System.Action<float>>(L, 1)) 
                {
                    System.Action<float> _checkUpdate = translator.GetDelegate<System.Action<float>>(L, 1);
                    
                        var gen_ret = FuXi.FxManager.FxCheckUpdateCo( _checkUpdate );
                        translator.Push(L, gen_ret);
                    
                    
                    
                    return 1;
                }
                if(gen_param_count == 0) 
                {
                    
                        var gen_ret = FuXi.FxManager.FxCheckUpdateCo(  );
                        translator.Push(L, gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + gen_e);
            }
            
            return LuaAPI.luaL_error(L, "invalid arguments to FuXi.FxManager.FxCheckUpdateCo!");
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_FxCheckDownloadSizeCo_xlua_st_(RealStatePtr L)
        {
		    try {
            
                ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            
			    int gen_param_count = LuaAPI.lua_gettop(L);
            
                if(gen_param_count == 2&& LuaTypes.LUA_TBOOLEAN == LuaAPI.lua_type(L, 1)&& translator.Assignable<System.Action<float>>(L, 2)) 
                {
                    bool _containsPackage = LuaAPI.lua_toboolean(L, 1);
                    System.Action<float> _checkDownload = translator.GetDelegate<System.Action<float>>(L, 2);
                    
                        var gen_ret = FuXi.FxManager.FxCheckDownloadSizeCo( _containsPackage, _checkDownload );
                        translator.Push(L, gen_ret);
                    
                    
                    
                    return 1;
                }
                if(gen_param_count == 1&& LuaTypes.LUA_TBOOLEAN == LuaAPI.lua_type(L, 1)) 
                {
                    bool _containsPackage = LuaAPI.lua_toboolean(L, 1);
                    
                        var gen_ret = FuXi.FxManager.FxCheckDownloadSizeCo( _containsPackage );
                        translator.Push(L, gen_ret);
                    
                    
                    
                    return 1;
                }
                if(gen_param_count == 0) 
                {
                    
                        var gen_ret = FuXi.FxManager.FxCheckDownloadSizeCo(  );
                        translator.Push(L, gen_ret);
                    
                    
                    
                    return 1;
                }
                if(gen_param_count == 2&& translator.Assignable<string[]>(L, 1)&& translator.Assignable<System.Action<float>>(L, 2)) 
                {
                    string[] _packages = (string[])translator.GetObject(L, 1, typeof(string[]));
                    System.Action<float> _checkDownload = translator.GetDelegate<System.Action<float>>(L, 2);
                    
                        var gen_ret = FuXi.FxManager.FxCheckDownloadSizeCo( _packages, _checkDownload );
                        translator.Push(L, gen_ret);
                    
                    
                    
                    return 1;
                }
                if(gen_param_count == 1&& translator.Assignable<string[]>(L, 1)) 
                {
                    string[] _packages = (string[])translator.GetObject(L, 1, typeof(string[]));
                    
                        var gen_ret = FuXi.FxManager.FxCheckDownloadSizeCo( _packages );
                        translator.Push(L, gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + gen_e);
            }
            
            return LuaAPI.luaL_error(L, "invalid arguments to FuXi.FxManager.FxCheckDownloadSizeCo!");
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_FxCheckDownloadCo_xlua_st_(RealStatePtr L)
        {
		    try {
            
                ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            
			    int gen_param_count = LuaAPI.lua_gettop(L);
            
                if(gen_param_count == 2&& translator.Assignable<FuXi.DownloadInfo>(L, 1)&& translator.Assignable<System.Action<float>>(L, 2)) 
                {
                    FuXi.DownloadInfo _downloadInfo;translator.Get(L, 1, out _downloadInfo);
                    System.Action<float> _checkDownload = translator.GetDelegate<System.Action<float>>(L, 2);
                    
                        var gen_ret = FuXi.FxManager.FxCheckDownloadCo( _downloadInfo, _checkDownload );
                        translator.Push(L, gen_ret);
                    
                    
                    
                    return 1;
                }
                if(gen_param_count == 1&& translator.Assignable<FuXi.DownloadInfo>(L, 1)) 
                {
                    FuXi.DownloadInfo _downloadInfo;translator.Get(L, 1, out _downloadInfo);
                    
                        var gen_ret = FuXi.FxManager.FxCheckDownloadCo( _downloadInfo );
                        translator.Push(L, gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + gen_e);
            }
            
            return LuaAPI.luaL_error(L, "invalid arguments to FuXi.FxManager.FxCheckDownloadCo!");
            
        }
        
        
        
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _g_get_RuntimeMode(RealStatePtr L)
        {
		    try {
                ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
			    translator.Push(L, FuXi.FxManager.RuntimeMode);
            } catch(System.Exception gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + gen_e);
            }
            return 1;
        }
        
        
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _s_set_RuntimeMode(RealStatePtr L)
        {
		    try {
                ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
			FuXi.RuntimeMode gen_value;translator.Get(L, 1, out gen_value);
				FuXi.FxManager.RuntimeMode = gen_value;
            
            } catch(System.Exception gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + gen_e);
            }
            return 0;
        }
        
		
		
		
		
    }
}
